package com.gwcworld.util;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.fileupload.DiskFileUpload;
import org.apache.commons.fileupload.FileItem;

public class FormRequest {
	
	private LinkedHashMap itemlistdata = null;
	private transient LinkedHashMap itemdata = null;

	/**
	 * 
	 * @param request
	 * @throws Exception
	 */
	public FormRequest(HttpServletRequest request) throws Exception {
		process(request);
	}

	/**
	 * 
	 * @param request
	 * @throws Exception
	 */
	private void process(HttpServletRequest request) throws Exception {
		// content type multipart
		if (request.getContentType() != null
				&& request.getContentType().toLowerCase()
						.indexOf("multipart/form-data") != -1) {
			List l = new DiskFileUpload().parseRequest(request);

			if (itemlistdata == null)
				itemlistdata = new LinkedHashMap();

			// clean previous values
			for (int i = 0; i < l.size(); i++) {
				FileItem item = (FileItem) l.get(i);
				itemlistdata.remove(item.getFieldName());
			}

			// process
			for (int i = 0; i < l.size(); i++) {
				FileItem item = (FileItem) l.get(i);
				if (item.getFieldName() != null) {
					FormRequestItem el = null;

					if (item.isFormField()) {
						el = new FormRequestItem(item.getString());
					} else {
						byte[] filedata = item.get();
						String filename = null;
						String s = item.getName();
						if (s.indexOf("/") != -1)
							filename = s.substring(s.lastIndexOf("/") + 1,
									s.length());
						else
							filename = s.substring(s.lastIndexOf("\\") + 1,
									s.length());
						String contenttype = item.getContentType();
						el = new FormRequestItem(filename, filedata,
								contenttype);
					}

					if (itemlistdata.get(item.getFieldName()) == null)
						itemlistdata
								.put(item.getFieldName(), new ArrayList());
					((ArrayList) itemlistdata.get(item.getFieldName()))
							.add(el);
				}
			}

		}
		// content type normal
		else {
			itemlistdata = new LinkedHashMap();

			// clean previous values
			Enumeration params = request.getParameterNames();
			while (params.hasMoreElements()) {
				String param = (String) params.nextElement();
				itemlistdata.remove(param);
			}

			// process
			params = request.getParameterNames();
			while (params.hasMoreElements()) {
				String param = (String) params.nextElement();

				String[] values = request.getParameterValues(param);
				for (int i = 0; i < values.length; i++) {
					FormRequestItem el = new FormRequestItem(values[i]);
					if (itemlistdata.get(param) == null)
						itemlistdata.put(param, new ArrayList());
					((ArrayList) itemlistdata.get(param)).add(el);
				}
			}
		}
	}

	/**
	 * 
	 * @param name
	 * @return
	 */
	public String[] getItemNameList(String name) {
		ArrayList l = new ArrayList();
		Iterator iterator = itemlistdata.keySet().iterator();

		while (iterator.hasNext()) {
			Object key = iterator.next();
			Object value = itemlistdata.get(key);

			if (value != null) {
				l.add(key);
			}
		}

		String[] names = new String[l.size()];
		return (String[]) l.toArray(names);
	}

	/**
	 * 
	 * @return
	 */
	public Map getItemListMap() {
		return itemlistdata;
	}

	/**
	 * 
	 * @return
	 */
	public Map getItemMap() {
		if (itemdata == null) {
			itemdata = new LinkedHashMap();
			for (Iterator iter = itemlistdata.entrySet().iterator(); iter
					.hasNext();) {
				Map.Entry element = (Map.Entry) iter.next();
				String key = (String) element.getKey();
				ArrayList value = (ArrayList) element.getValue();
				if (value != null && value.size() > 0) {
					itemdata.put(key, value.get(0));
				}
			}

		}
		return itemdata;
	}

	/**
	 * 
	 * @param name
	 * @return
	 */
	public FormRequestItem getItem(String name) {
		ArrayList l = (ArrayList) itemlistdata.get(name);
		if (l != null && l.size() > 0)
			return (FormRequestItem) l.get(0);
		else
			return null;
	}

	/**
	 * 
	 * @param name
	 * @return
	 */
	public FormRequestItem[] getItemList(String name) {
		ArrayList l = (ArrayList) itemlistdata.get(name);
		if (l != null) {
			FormRequestItem[] fri = new FormRequestItem[l.size()];
			return (FormRequestItem[]) l.toArray(fri);
		} else
			return new FormRequestItem[0];
	}
}
