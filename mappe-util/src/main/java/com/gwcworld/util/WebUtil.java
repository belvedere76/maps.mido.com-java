package com.gwcworld.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.BodyContent;

import net.sf.json.JSON;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;
import net.sf.json.JsonConfig;



public class WebUtil {
	
	/**
	 * set cache expire
	 * 
	 * @param response
	 */
	public static void setNoCache(HttpServletResponse response) {
		response.setDateHeader("Expires", 0);
		response.setHeader("Cache-Control", "no-cache");
		response.setHeader("Pragma", "no-cache");
	}

	/**
	 * 
	 * @param s
	 * @return
	 */
	public static String parameter2string(String s) {
		return ((s == null) ? "" : s);
	}

	/**
	 * 
	 * @param s
	 * @return
	 */
	public static int parameter2int(String s) {
		if (s == null)
			return 0;
		else
			try {
				return Integer.parseInt(s);
			} catch (Exception e) {
				return 0;
			}
	}

	public static double parameter2double(String s) {
		if (s == null)
			return 0;
		else
			try {
				return Double.parseDouble(s);
			} catch (Exception e) {
				return 0;
			}
	}

	/**
	 * 
	 * @param s
	 * @return
	 */
	public static long parameter2long(String s) {
		if (s == null)
			return 0;
		else
			try {
				return Long.parseLong(s);
			} catch (Exception e) {
				return 0;
			}

	}

	/**
	 * 
	 * @param s
	 * @param token
	 * @return
	 */
	public static long[] parameter2longArray(String s, String token) {
		long[] array = new long[0];
		if (s != null) {
			StringTokenizer st = new StringTokenizer(s, token);
			array = new long[st.countTokens()];
			int count = 0;
			while (st.hasMoreTokens()) {
				try {
					array[count] = Long.parseLong(st.nextToken().trim());
					count++;
				} catch (Exception e) {
					array = new long[0];
				}
			}
		}
		return array;
	}

	/**
	 * 
	 * @param s
	 * @return
	 */
	public static boolean parameter2boolean(String s) {
		return ((s == null) ? false : (s.toLowerCase().trim().equals("true")));
	}
	
	public static boolean parameter2boolean(int i) {
		return (i == 1 ? true : false);
	}

	public static Boolean parameter2Boolean(String s) {
		return ((s == null || s.equalsIgnoreCase("null")) ? null : new Boolean(
				s));
	}

	/**
	 * 
	 * @param ds
	 * @return
	 */
	public static Date parameter2date(String ds, String dateformat) {
		Date dt;
		SimpleDateFormat sdf = new SimpleDateFormat(dateformat);
		sdf.setLenient(false);
		try {
			dt = sdf.parse(ds);
		} catch (Exception e) {
			dt = null;
		}
		return dt;
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public static String requestAsText(HttpServletRequest request) throws Exception {
		return new String(requestAsBytes(request));
	}

	public static String getJspContent(String jsppath, PageContext pageContext, JspWriter out) throws Throwable {
		BodyContent bc;

		bc = pageContext.pushBody();
		pageContext.include(jsppath);
		String retval = new String(bc.getString().getBytes());
		out = pageContext.popBody();

		return retval;
	}

	public static byte[] requestAsBytes(HttpServletRequest request) throws Exception {
		InputStream is = request.getInputStream();
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		byte[] b = new byte[1024];
		int p = 0;
		while ((p = is.read(b)) != -1) {
			baos.write(b, 0, p);
		}
		return baos.toByteArray();
	}
	
	public static void respons2Json(HttpServletResponse response, Object json) throws IOException {
		JSONObject jsonObject = JSONObject.fromObject(json);
		response.setContentType("application/json");
		response.getWriter().print(jsonObject.toString());
	}
	
	public static void respons2JsonArray(HttpServletResponse response, Object json) throws IOException {
		JSONArray jsonObject = JSONArray.fromObject(json);
		response.setContentType("application/json");
		response.getWriter().print(jsonObject.toString());
	}
	
	public static void respons2JsonArrayHibernate(HttpServletResponse response, Object json) throws IOException {
		JsonConfig jsonConfig = new JsonConfig();  
		jsonConfig.setExcludes(new String[]{"delegate"});  
		jsonConfig.setExcludes(new String[]{"transactionTimeout"});
		jsonConfig.registerJsonBeanProcessor(org.hibernate.proxy.HibernateProxy.class, new HibernateJsonBeanProcessor());
		jsonConfig.setJsonBeanProcessorMatcher(new HibernateJsonBeanProcessorMatcher());
		JSON jsonObject = JSONSerializer.toJSON(json,jsonConfig);
		response.setContentType("application/json");
		response.getWriter().print(jsonObject.toString());
	}
}
