package com.gwcworld.util;


import java.io.IOException;
import java.io.StringWriter;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.exception.ParseErrorException;
import org.apache.velocity.exception.ResourceNotFoundException;

public class MailUtil {

	private String smtpHost;
	private String fromAddress;
	private List<String> ccAddresses;
	private String usernameAuth;
	private String passwordAuth;
	private String smtpPort;
	private Boolean auth;
	
	
	
	public void sendMessage(String toAddress, String subject, String velocityTemplate, Map<String, Object> params) throws AddressException, MessagingException, IOException, ParseErrorException, ResourceNotFoundException, Exception {
		
		VelocityEngine ve = new VelocityEngine();
		ve.setProperty("resource.loader", "class");
		ve.setProperty("class.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
		ve.setProperty("runtime.log.logsystem.log4j.logger", "Log4JLogChute");
		ve.init();
		
		Template template = ve.getTemplate(velocityTemplate);
		VelocityContext velocityContext = new VelocityContext();
		
		for( String key : params.keySet() ){
			velocityContext.put(key, params.get(key));
		}
		
		StringWriter stringWriter = new StringWriter();
        template.merge(velocityContext, stringWriter);
        String body = stringWriter.toString();
		
		Session session = null;
		
		if( auth ){
			
			Properties props = new Properties();
			props.setProperty("mail.transport.protocol", "smtp");
			props.setProperty("mail.host", smtpHost);
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.port", smtpPort!=null?smtpPort:"465");
//			props.put("mail.smtp.socketFactory.port", smtpPort!=null?smtpPort:"465");
//			props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
//			props.put("mail.smtp.socketFactory.fallback", "false");
			props.setProperty("mail.smtp.quitwait", "false");
			
			session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(usernameAuth, passwordAuth);
				}
			});			
		} else {
			Properties p = System.getProperties();
			p.put("mail.smtp.host", smtpHost);
			session = Session.getInstance(p, null);
		}
		
		MimeMessage message = new MimeMessage(session);
		message.setFrom(new InternetAddress(fromAddress));
		message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toAddress));
		if (ccAddresses != null) {
			Address[] addresses = new Address[ccAddresses.size()];
			int i = 0;
			for (String address : ccAddresses) {
				addresses[i++] = new InternetAddress(address);
			}
			message.setRecipients(Message.RecipientType.CC, addresses);
		}
		message.setSubject(subject);
		message.setContent(body, "text/html");
		Transport.send(message);
	}

	public void setAuth(Boolean auth) {
		this.auth = auth;
	}

	public void setCcAddresses(List<String> ccAddresses) {
		this.ccAddresses = ccAddresses;
	}

	public void setFromAddress(String fromAddress) {
		this.fromAddress = fromAddress;
	}

	public void setSmtpHost(String smtpHost) {
		this.smtpHost = smtpHost;
	}

	public void setPasswordAuth(String passwordAuth) {
		this.passwordAuth = passwordAuth;
	}

	public void setSmtpPort(String smtpPort) {
		this.smtpPort = smtpPort;
	}

	public void setUsernameAuth(String usernameAuth) {
		this.usernameAuth = usernameAuth;
	}

	
}
