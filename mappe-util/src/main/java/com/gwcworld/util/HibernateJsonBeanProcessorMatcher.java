package com.gwcworld.util;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.sf.json.processors.JsonBeanProcessorMatcher;

public class HibernateJsonBeanProcessorMatcher extends JsonBeanProcessorMatcher {

private static Logger log = LoggerFactory.getLogger(HibernateJsonBeanProcessorMatcher.class);

	@Override
	public Object getMatch(Class target, Set set) {
	    if (target.getName().contains("$$EnhancerByCGLIB$$")) {
	        log.warn("Found Lazy-References in Hibernate object "
	                + target.getName());
	        return org.hibernate.proxy.HibernateProxy.class;
	    }
	    return DEFAULT.getMatch(target, set);
	}
}
