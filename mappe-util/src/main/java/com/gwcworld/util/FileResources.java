package com.gwcworld.util;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;

public class FileResources {

	private static Logger logger = Logger.getLogger(FileResources.class);

	public static void fileDelete(String filename, String path)
			throws Exception {
		try {
			localFileDelete(filename, path);
		} catch (Exception e) {
			logger.error("Error deleting file " + filename);
			logger.error(e);
			// throw new Exception(e.getMessage());
		}
	}

	private static void localFileDelete(String filename, String path)
			throws Exception {

		try {
			File file2delete = new File(path + filename);
			if (!file2delete.delete())
				logger.warn("Negative result while deleting file " + path
						+ filename + " (FTLE mode)");
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error during local delete of file " + filename);
		}
	}

	public static String saveFile(FormRequest formrequest, FormRequestItem item, String destPath) throws IOException {

		String fileName = null;
		String prefix = "";

		prefix = Md5Util.encript(item.getFilename());

		if (item != null && !item.getFilename().trim().equals("")) {
			fileName = prefix + "_" + item.getFilename();
			byte[] data = item.getFiledata();

			FileOutputStream fos = new FileOutputStream(destPath + fileName);
			fos.write(data);
			fos.flush();
			fos.close();
		}
		return fileName;
	}

	public static void localFileCopy(File f, String destPath) {

		try {
			DataInputStream dis = new DataInputStream(new BufferedInputStream(
					new FileInputStream(f)));
			byte[] buff = new byte[(int) f.length()];
			dis.readFully(buff);
			dis.close();

			FileOutputStream fos = new FileOutputStream(destPath + f.getName());
			fos.write(buff);
			fos.flush();
			fos.close();
		} catch (Exception e) {
			logger.error("Error during localFileCopy: " + e);
			e.printStackTrace();
		}
	}

	/**
	 * Recursively walk a directory tree and return a List of all Files found;
	 * the List is sorted using File.compareTo().
	 * 
	 * @param aStartingDir
	 *            is a valid directory, which can be read.
	 */
	static public List<File> getFileListing(File aStartingDir, boolean recursive)
			throws FileNotFoundException {
		validateDirectory(aStartingDir);
		List<File> result = getFileListingNoSort(aStartingDir, recursive);
		Collections.sort(result);
		return result;
	}

	static private List<File> getFileListingNoSort(File aStartingDir,
			boolean recursive) throws FileNotFoundException {
		List<File> result = new ArrayList<File>();
		File[] filesAndDirs = aStartingDir.listFiles();
		List<File> filesDirs = Arrays.asList(filesAndDirs);
		for (File file : filesDirs) {
			if (file.isFile())
				result.add(file);
			if (!file.isFile() && recursive) {
				// must be a directory recursive call!
				List<File> deeperList = getFileListingNoSort(file, recursive);
				result.addAll(deeperList);
			}
		}
		return result;
	}

	/**
	 * Directory is valid if it exists, does not represent a file, and can be
	 * read.
	 */
	static private void validateDirectory(File aDirectory)
			throws FileNotFoundException {
		if (aDirectory == null) {
			throw new IllegalArgumentException("Directory should not be null.");
		}
		if (!aDirectory.exists()) {
			throw new FileNotFoundException("Directory does not exist: "
					+ aDirectory);
		}
		if (!aDirectory.isDirectory()) {
			throw new IllegalArgumentException("Is not a directory: "
					+ aDirectory);
		}
		if (!aDirectory.canRead()) {
			throw new IllegalArgumentException("Directory cannot be read: "
					+ aDirectory);
		}
	}

}
