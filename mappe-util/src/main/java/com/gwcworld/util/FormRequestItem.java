package com.gwcworld.util;

import java.io.Serializable;

public class FormRequestItem implements Serializable {
	
	public final static int TYPE_TEXT = 1;
	public final static int TYPE_FILE = 2;

	private int type;
	private String value;
	private byte[] filedata;
	private String filename;
	private String contenttype;

	public FormRequestItem(String value) {
		this.type = TYPE_TEXT;
		this.value = value;
	}

	public FormRequestItem(String filename, byte[] filedata, String contenttype) {
		this.type = TYPE_FILE;
		this.filename = filename;
		this.filedata = filedata;
		this.contenttype = contenttype;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public byte[] getFiledata() {
		return filedata;
	}

	public void setFiledata(byte[] filedata) {
		this.filedata = filedata;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getContenttype() {
		return contenttype;
	}

	public void setContenttype(String contenttype) {
		this.contenttype = contenttype;
	}

	
}
