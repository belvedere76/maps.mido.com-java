package com.gwcworld.util;

import java.util.Date;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.log4j.Logger;



public class EmailUtil {
	
	private static Logger m_log = Logger.getLogger(EmailUtil.class);
	private static Properties props;
	private static String smtpUser;
	private static String smtpPasswd;
	
	
	public static boolean sendEmail(String aFromEmailAddr, String aToEmailAddr, String aSubject, String aBody, EmailProperties emailProperties) throws Exception {
		return sendEmail(aFromEmailAddr, new String[] {aToEmailAddr}, null, aSubject, aBody, emailProperties);
	}
	
	public static boolean sendEmail(String aFromEmailAddr, String aToEmailAddr, String[] ccEmailAddress, String aSubject, String aBody, EmailProperties emailProperties) throws Exception {
		return sendEmail(aFromEmailAddr, new String[] {aToEmailAddr}, ccEmailAddress, aSubject, aBody, emailProperties);
	}
	
	private static boolean sendEmail(String aFromEmailAddr, String[] aToEmailAddr, String[] ccEmailAddress, String aSubject, String aBody, EmailProperties emailProperties) throws Exception {

		props = new Properties();
		props.put("mail.transport.protocol", "smtp");
		props.put("mail.smtp.host", emailProperties.getHost());
        props.put("mail.smtp.auth", String.valueOf(emailProperties.isAuthenticated()));
        smtpUser = emailProperties.getSmtpUser();
        smtpPasswd = emailProperties.getSmtpPasswd();
		
		boolean sent = true;
		String from = aFromEmailAddr;
		String[] to = aToEmailAddr;
		String[] cc = ccEmailAddress;
		String subject = aSubject;
		
		Authenticator auth = props.get("mail.smtp.auth").equals("true")?new SMTPAuthenticator():null;
		Session session = Session.getDefaultInstance(props, auth);

		MimeMessage message = new MimeMessage(session);

		try {
			
			if(from != null)
				message.setFrom(new InternetAddress(from));
			else
				message.setFrom();

			if((to != null) && (to.length > 0)){
				for (int i=0; i < to.length; i++) {
					message.addRecipient(Message.RecipientType.TO, new InternetAddress(aToEmailAddr[i]));
				}
			}
			if((cc != null) && (cc.length > 0)){
				for (int i=0; i < cc.length; i++) {
					message.addRecipient(Message.RecipientType.BCC, new InternetAddress(ccEmailAddress[i]));
				}
			}
			
			message.setSubject(subject);
			message.setText(aBody);
			message.setContent(aBody, "text/html");
			message.setSentDate(new Date());

			Transport.send(message);
			
		} catch (MessagingException me) {
			sent = false;
			m_log.error("Cannot send email. " + me);
			throw new Exception("MessagingException: " + me.getMessage(), me);
		} catch (Exception e) {
			sent = false;
			m_log.error("Cannot send email. " + e);
			throw new Exception("Exception: " + e.getMessage(), e);
		}
		return sent;
	}
	
	 private static class SMTPAuthenticator extends javax.mail.Authenticator {
	       
		 public PasswordAuthentication getPasswordAuthentication() {
	     	String username = smtpUser;
	        String password = smtpPasswd;
	        return new PasswordAuthentication(username, password);
	     }
	 }

	
}


