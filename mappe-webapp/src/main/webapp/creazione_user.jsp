<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<!DOCTYPE html>
<html>
  <%@ include file="head.jsp" %>
  <body>
    <c:import url="header.do"/>
    <section>
      <div id="container">
        <c:import url="leftColumn.do">
      		<c:param name="selectedMenu" value="usersList"/>
      		<c:param name="showUser" value="${false}"/>
      		<c:param name="showProjects" value="${true}"/>
      		<c:param name="showExhibitors" value="${false}"/>
      	</c:import>
        <div id="rightColumn">
          <div id="userCreate">
            <h1>Crea nuovo utente</h1>
            
            <c:choose>
            	<c:when test="${not empty result and result eq true}">
            		<p>Utente creato con successo. E' stata inviata una e-mail all'indirizzo specificato contenente un link di conferma necessario per completare la registrazione.</p>
            	</c:when>
            	<c:when test="${not empty result and result eq false}">
            		<p>Spiacenti, si &egrave; verificato un problema durante la registrazione del nuovo utente. Si prega di riprovare pi&ugrave; tardi.</p>
            	</c:when>
            	<c:otherwise>
            		<c:set var="creationId"/>
		            <c:choose>
		            	<c:when test="${not empty userView}"><c:set var="creationId" value="${userView.id}"/></c:when>
		               	<c:otherwise><c:set var="creationId" value="0"/></c:otherwise>	
		           	</c:choose>
		            <form action="<c:url value="it/editUser.do"/><c:out value="${creationId}"/>" method="POST">
		            	<input type="hidden" name="submitted" value="true"/>
		            	<c:if test="${mappeUser.userGroup.level != 1}">
		            		<input type="hidden" name="groupId" value="<c:out value="${mappeUser.userGroup.id}"/>"/>
		            		<input type="hidden" name="customerId" value="<c:out value="${mappeUser.customer.id}"/>"/>
		            	</c:if>
		           		<input type="hidden" name="userId" value="<c:out value="${creationId}"/>"/>
		              <ul>
		                <li class="row">
		                  <label>Email</label>
		                  <input type="text" name="email" placeholder="E-mail" value="<c:if test="${not empty userView}"><c:out value="${userView.email}"/></c:if>">
		                </li>
		                <li class="row">
		                  <label>Password</label>
		                  <input type="password" name="password" placeholder="Password" value="<c:if test="${not empty userView}"><c:out value="${userView.password}"/></c:if>">
		                </li>
		                <li class="br"></li>
		                <c:if test="${mappeUser.userGroup.level == 1}">
		                	<li class="row">
			                  <label>Cliente di appartenenza</label>
			                  <select name="customerId">
			                  	<c:forEach items="${customers}" var="customer">
			                  		<option value='<c:out value="${customer.id}"/>'><c:out value="${customer.name}"/></option>
			                  	</c:forEach>
			                  </select>
			                </li>
		                </c:if>
		                <li class="row">
		                  <label>Gruppo di appartenenza</label>
		                  <select name="groupId">
		                    <c:forEach items="${groups}" var="group">
		                    	<c:if test="${mappeUser.userGroup.level == 1 || (mappeUser.userGroup.level != 1 && (mappeUser.userGroup.level < group.level))}">
		                  			<option value='<c:out value="${group.id}"/>'><c:out value="${group.name}"/></option>
		                    	</c:if>
		                  	</c:forEach>
		                  </select>
		                </li>
		                <li class="button">
		                	<c:set var="submitLabel" value=""/>
		                	<c:choose>
		                		<c:when test="${not empty userView}">
		                			<c:set var="submitLabel" value="Aggiorna profilo"/>
		                		</c:when>
		                		<c:otherwise>
		                			<c:set var="submitLabel" value="Crea nuovo utente"/>
		                		</c:otherwise>
		                	</c:choose>
		                  <input type="submit" value="<c:out value="${submitLabel}"/>">
		                </li>
		              </ul>
		            </form>
            	</c:otherwise>
            </c:choose>         
          </div>
        </div>
      </div>
    </section>
  </body>
</html>