<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<!DOCTYPE html>
<html>
  <%@ include file="head.jsp" %>
  <body>
    <header>
		<div id="header">
			<a id="logo" href="<c:url value="it/login.do"/>" class="col2"><img src="<c:url value="img/logo.png"/>" alt="MIDO"></a>
			<div id="headermenu" class="col9">
				<div>
					<a href="<c:url value="it/login.do"/>">Accedi</a>
				</div>
			</div>
			<div id="headerextra" class="col1"></div>
		</div>
	</header>
    <section>
      <div id="container">
        <div id="leftColumn">
          <div id="loggedOut"><img src="<c:url value="img/login.png"/>">
            <h1>Login</h1>
            <!--p
            <input type="checkbox">&nbsp;Ricordami
            -->
            <p><a href="<c:url value="it/login.do"/>">Login</a></p>
          </div>
        </div>
        <div id="rightColumn">
        	<h1>Attivazione utente</h1>
          <c:choose>
           	<c:when test="${not empty result and result eq true}">
           		<p>Utente attivato con successo. Da questo momento puoi effettuare il login inserendo la tua e-mail e la tua password.</p>
           	</c:when>
           	<c:when test="${not empty result and result eq false}">
           		<p>Spiacenti, l'utente associato a questo codice di conferma &egrave; insesistente oppure &egrave; gi&agrave; stato attivato.</p>
           	</c:when>
          </c:choose>
        </div>
      </div>
    </section>
  </body>
</html>