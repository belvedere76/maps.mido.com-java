<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<header>
	<div id="header">
		<c:choose>
			<c:when test="${isDesigner eq true}"><div id="logodiv" class="col2"><img src="<c:url value="img/logo.png"/>" alt="MIDO"></div></c:when>
			<c:otherwise><div id="logodiv" class="col2"><a id="logo" href="<c:url value="it/login.do"/>"><img src="<c:url value="img/logo.png"/>" alt="MIDO"></a></div></c:otherwise>
		</c:choose>
		<div id="headermenu" class="col9">
			<c:choose>
				<c:when test="${not empty mappeUser and empty isDesigner}">
			          <div class="dropdown">
			            <div class="selector">Bentornato&nbsp;<span><c:out value="${mappeUser.email}"/></span></div>
			            <div class="menu">
			              <ul>
			               	<li><a href="<c:url value="it/editUser.do"/>${mappeUser.id}">Profilo</a></li>
			              </ul>
			            </div>
			          </div>
			        </div>
			        <div id="headerextra" class="col1"><a href="<c:url value="it/logout.do"/>">Esci</a></div>
				</c:when>
				<c:when test="${not empty mappeUser and isDesigner eq true}">
					<div>Bentornato&nbsp;<span><c:out value="${mappeUser.email}"/></span></div>
				</c:when>
				<c:otherwise>
					<div>
						<a href="<c:url value="it/login.do"/>">Accedi</a>
					</div>
				</c:otherwise>
			</c:choose>
		</div>
		<div id="headerextra" class="col1"></div>
	</div>
</header>