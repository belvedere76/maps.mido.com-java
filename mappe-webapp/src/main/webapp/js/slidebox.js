(function() {
  var _init = function() {
    var slideboxes = document.querySelectorAll('div.slidebox');

    for(var i in slideboxes) if(typeof slideboxes[i] === 'object') {
      var s = slideboxes[i];  
      s.addEventListener('click', function(evt) {
        evt.preventDefault();
        evt.stopPropagation();
        var input = this.querySelector('input');
        input.checked = !input.checked;
      })
    }
  };

  if(document.readyState === 'complete')
    _init();
  else
    document.onreadystatechange = function() {
      if(document.readyState === 'complete')
        _init();
    }
}());
