<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf8">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <link rel="shortcut icon" type="image/jpeg" href="<c:url value="img/favicon.jpg"/>"/>
    <title>Maps 2.0</title>
    <script src="<c:url value="build-lite.js"/>"></script>
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Lato:300,700" type="text/css">
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="<c:url value="css/designer.css"/>" type="text/css">
    <link rel="stylesheet" href="<c:url value="css/lite.css"/>" type="text/css">
  </head>
  <body data-id-mappa="<c:choose><c:when test="${not empty map}"><c:out value="${map.id}"/></c:when><c:otherwise>0</c:otherwise></c:choose>" data-id-stand="<c:out value="${stand.id}"/>" data-nome-mappa="<c:out value="${map.name}"/>" data-token="" data-api="http://maps.mido.com">
    <c:import url="header.jsp">
    	<c:param name="isDesigner" value="${true}"/>
    </c:import>
    <header>
	<div id="header">
		<div id="logodiv" class="col2"><img src="<c:url value="img/logo.png"/>" alt="MIDO"></div>
			<div id="headermenu" class="col9">
				<div></div>
			</div>
			<div id="headerextra" class="col1"></div>
		</div>
	</header>
    <section id="designer">
      <div id="svgContainer">
        <svg id="svg" xmlns="http://www.w3.org/2000/svg" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink"></svg>
      </div>
      <div id="statusBar">
      	<div id="screenshotTool"></div>
        <div id="zoomControl">
          <div id="zoomPin" class="fa-caret-down"></div>
        </div>
        <div id="powered">Powered by GWC</div>
      </div>
    </section>
  </body>
</html>