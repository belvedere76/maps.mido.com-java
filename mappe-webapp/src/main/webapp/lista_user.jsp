<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<!DOCTYPE html>
<html>
  <%@ include file="head.jsp" %>
  <body>
    <c:import url="header.do"/>
    <section>
      <div id="container">
        <c:import url="leftColumn.do">
      		<c:param name="selectedMenu" value="usersList"/>
      		<c:param name="showUser" value="${true}"/>
      		<c:param name="showProjects" value="${true}"/>
      		<c:param name="showExhibitors" value="${true}"/>
      		<c:param name="customerId" value="${customer.id}"/>
      	</c:import>
        <div id="rightColumn">
          <div id="exhibitorsList">
            <div class="alphabet">
              <div><a href="<c:url value="it/usersList.do"/><c:out value="${customer.id}"/>/a" <c:if test="${name eq 'a'}">class="current"</c:if>>A</a>
              </div>
              <div><a href="<c:url value="it/usersList.do"/><c:out value="${customer.id}"/>/b" <c:if test="${name eq 'b'}">class="current"</c:if>>B</a>
              </div>
              <div><a href="<c:url value="it/usersList.do"/><c:out value="${customer.id}"/>/c" <c:if test="${name eq 'c'}">class="current"</c:if>>C</a>
              </div>
              <div><a href="<c:url value="it/usersList.do"/><c:out value="${customer.id}"/>/d" <c:if test="${name eq 'd'}">class="current"</c:if>>D</a>
              </div>
              <div><a href="<c:url value="it/usersList.do"/><c:out value="${customer.id}"/>/e" <c:if test="${name eq 'e'}">class="current"</c:if>>E</a>
              </div>
              <div><a href="<c:url value="it/usersList.do"/><c:out value="${customer.id}"/>/f" <c:if test="${name eq 'f'}">class="current"</c:if>>F</a>
              </div>
              <div><a href="<c:url value="it/usersList.do"/><c:out value="${customer.id}"/>/g" <c:if test="${name eq 'g'}">class="current"</c:if>>G</a>
              </div>
              <div><a href="<c:url value="it/usersList.do"/><c:out value="${customer.id}"/>/h" <c:if test="${name eq 'h'}">class="current"</c:if>>H</a>
              </div>
              <div><a href="<c:url value="it/usersList.do"/><c:out value="${customer.id}"/>/i" <c:if test="${name eq 'i'}">class="current"</c:if>>I</a>
              </div>
              <div><a href="<c:url value="it/usersList.do"/><c:out value="${customer.id}"/>/j" <c:if test="${name eq 'j'}">class="current"</c:if>>J</a>
              </div>
              <div><a href="<c:url value="it/usersList.do"/><c:out value="${customer.id}"/>/k" <c:if test="${name eq 'k'}">class="current"</c:if>>K</a>
              </div>
              <div><a href="<c:url value="it/usersList.do"/><c:out value="${customer.id}"/>/l" <c:if test="${name eq 'l'}">class="current"</c:if>>L</a>
              </div>
              <div><a href="<c:url value="it/usersList.do"/><c:out value="${customer.id}"/>/m" <c:if test="${name eq 'm'}">class="current"</c:if>>M</a>
              </div>
              <div><a href="<c:url value="it/usersList.do"/><c:out value="${customer.id}"/>/n" <c:if test="${name eq 'n'}">class="current"</c:if>>N</a>
              </div>
              <div><a href="<c:url value="it/usersList.do"/><c:out value="${customer.id}"/>/o" <c:if test="${name eq 'o'}">class="current"</c:if>>O</a>
              </div>
              <div><a href="<c:url value="it/usersList.do"/><c:out value="${customer.id}"/>/p" <c:if test="${name eq 'p'}">class="current"</c:if>>P</a>
              </div>
              <div><a href="<c:url value="it/usersList.do"/><c:out value="${customer.id}"/>/q" <c:if test="${name eq 'q'}">class="current"</c:if>>Q</a>
              </div>
              <div><a href="<c:url value="it/usersList.do"/><c:out value="${customer.id}"/>/r" <c:if test="${name eq 'r'}">class="current"</c:if>>R</a>
              </div>
              <div><a href="<c:url value="it/usersList.do"/><c:out value="${customer.id}"/>/s" <c:if test="${name eq 's'}">class="current"</c:if>>S</a>
              </div>
              <div><a href="<c:url value="it/usersList.do"/><c:out value="${customer.id}"/>/t" <c:if test="${name eq 't'}">class="current"</c:if>>T</a>
              </div>
              <div><a href="<c:url value="it/usersList.do"/><c:out value="${customer.id}"/>/u" <c:if test="${name eq 'u'}">class="current"</c:if>>U</a>
              </div>
              <div><a href="<c:url value="it/usersList.do"/><c:out value="${customer.id}"/>/v" <c:if test="${name eq 'v'}">class="current"</c:if>>V</a>
              </div>
              <div><a href="<c:url value="it/usersList.do"/><c:out value="${customer.id}"/>/w" <c:if test="${name eq 'w'}">class="current"</c:if>>W</a>
              </div>
              <div><a href="<c:url value="it/usersList.do"/><c:out value="${customer.id}"/>/x" <c:if test="${name eq 'x'}">class="current"</c:if>>X</a>
              </div>
              <div><a href="<c:url value="it/usersList.do"/><c:out value="${customer.id}"/>/y" <c:if test="${name eq 'y'}">class="current"</c:if>>Y</a>
              </div>
              <div><a href="<c:url value="it/usersList.do"/><c:out value="${customer.id}"/>/z" <c:if test="${name eq 'z'}">class="current"</c:if>>Z</a>
              </div>
            </div>
            <table>
            <c:forEach items="${users}" var="user">
            	<tr>
	            	<td><c:out value="${user.email}"/></td>
	                <td><a href="<c:url value="it/editUser.do"/><c:out value="${user.id}"/>">Anagrafica</a></td>
	                <td><c:out value="${user.userGroup.name}"/></td>
	                <td><c:if test="${user.active eq false}">Non </c:if>Abilitato</td>
	            </tr>
            </c:forEach>
            </table>
            <div class="boxHeader" style="text-align:right">
	        	<div class="action"><a href="<c:url value="it/editUser.do"/>0">Crea utente</a></div>
	        </div>
          </div>
        </div>
      </div>
    </section>
  </body>
</html>