<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<div id="leftColumn">
  <div id="loggedIn"><img src="<c:url value="img/login.png"/>">
  	
    <h1><c:out value="${shortUsername}"/></h1>
    <ul>
      <li class="profilo"><a href="<c:url value="it/editUser.do"/><c:out value="${mappeUser.id}"/>" <c:if test="${selectedMenu eq 'viewUser'}">class="current"</c:if>>Profilo</a></li>
      <c:if test="${mappeUser.userGroup.level <= 2}">
      		<c:if test="${showUser eq true}">
      			<li class="utenti"><a href="<c:url value="it/usersList.do"/><c:out value="${customerId}"/>/all" <c:if test="${selectedMenu eq 'usersList'}">class="current"</c:if>>Utenti</a></li>
      		</c:if>
	      	<c:if test="${showProjects eq true}">
	      		<li class="progetti"><a href="<c:url value="it/projectsList.do"/>" <c:if test="${selectedMenu eq 'projectsList'}">class="current"</c:if>>Progetti</a></li>
	      	</c:if>
      </c:if>
      <c:if test="${showExhibitors eq true}">
      	<li class="espositori"><a href="<c:url value="it/exhibitorsList.do"/><c:out value="${customerId}"/>/all" <c:if test="${selectedMenu eq 'exhibitorsList'}">class="current"</c:if>>Espositori</a></li>
      </c:if>
    </ul>
  </div>
</div>