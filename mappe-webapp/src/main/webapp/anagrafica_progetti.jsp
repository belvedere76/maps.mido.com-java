<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<!DOCTYPE html>
<html>
  <%@ include file="head.jsp" %>
  <body>
    <c:import url="header.do"/>
    <section>
      <div id="container">
        <c:import url="leftColumn.do">
      		<c:param name="selectedMenu" value="projectsList"/>
      		<c:param name="showUser" value="${true}"/>
      		<c:param name="showProjects" value="${true}"/>
      		<c:param name="showExhibitors" value="${true}"/>
      		<c:param name="customerId" value="${customerId}"/>
      	</c:import>
        <div id="rightColumn">
          <div id="projectProfile"><a href="<c:url value="it/projectsList.do"/>" class="back">Torna</a>
            <h2><c:out value="${project.name}"/></h2>
            <p><b>Creato il:</b><span><fmt:formatDate value="${project.creationDate}" pattern="dd/MM/yyyy"/></span></p>
            <p><b>Ultima modifica:</b><span><fmt:formatDate value="${project.lastEdit}" pattern="dd/MM/yyyy"/></span></p>
            <div class="box">
           	<c:if test="${not empty allMapsByName}">
           		<div class="boxHeader">
                	<div class="title">Mappe</div>
              	</div>
              	<table class="boxBody">
	              	<c:forEach items="${allMapsByName}" var="mapsByName">              		
	              		<tr>
		                  <td><c:out value="${mapsByName[0].name}"/></td>
		                  <c:choose>
		                  	<c:when test="${not empty mapsByName and not empty mapsByName[1] and mapsByName[1].name != 'null'}">
		                  		<td class="dropdown">
				                  	<!--<a href="<c:url value="it/designer.do"/><c:out value="${project.id}"/>/<c:out value="${map.id}"/>">Ultimi salvataggi</a>-->
				                  	<a href="javascript:void(0);" style="text-decoration:none">Ultimi salvataggi</a>
									<div class="menu">
										<ul>
						              		<c:forEach items="${mapsByName}" var="map" varStatus="status">
												<li><a href="<c:url value="it/designer.do"/><c:out value="${project.id}"/>/<c:out value="${map.id}"/>" target="_blank"><fmt:formatDate value="${map.lastEdit}" pattern="dd/MM/yyyy - hh:mm"/></a></li>
						              		</c:forEach>
		              					</ul>
									</div>
				                 </td>
		                  	</c:when>
		                  	<c:otherwise>
		                  		<td></td>
		                  	</c:otherwise>
		                  </c:choose> 
		                  <td><a href="<c:url value="it/designer.do"/><c:out value="${project.id}"/>/<c:out value="${mapsByName[0].id}"/>" target="_blank">Apri mappa</a></td>
			            </tr>
	              	</c:forEach>
	              </table>
           	</c:if>
            <div><a href="<c:url value="it/designer.do"/><c:out value="${project.id}"/>/0" target="_blank">Disegna una nuova mappa</a></div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </body>
</html>