package com.gwcworld.webapp.web.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.gwcworld.core.bean.UserGroup;
import com.gwcworld.core.service.UserGroupService;
import com.gwcworld.util.WebUtil;
import com.gwcworld.webapp.util.Constants;

public class UserGroups implements Controller{
			
	private UserGroupService userGroupService;
			
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {

		ModelAndView mav = null;
		
		List<UserGroup> userGroups = userGroupService.getAll();
		
		response.addHeader(Constants.CORS_HEADER, "*");
		
		WebUtil.respons2JsonArray(response, userGroups);
		
		return mav;
	}

	public void setUserGroupService(UserGroupService userGroupService) {
		this.userGroupService = userGroupService;
	}	
	
}
