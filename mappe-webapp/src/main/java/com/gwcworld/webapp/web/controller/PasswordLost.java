package com.gwcworld.webapp.web.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.gwcworld.core.bean.User;
import com.gwcworld.core.service.UserService;
import com.gwcworld.util.MailUtil;
import com.gwcworld.webapp.util.Constants;

public class PasswordLost implements Controller{
	
	private MailUtil mailUtil;
	private String pwdLostTemplate;
	private UserService userService;
	private Logger logger = LoggerFactory.getLogger(PasswordLost.class);
			
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {

		ModelAndView mav = new ModelAndView(Constants.PASSWORD_LOST);
		
		boolean submitted = request.getParameter("submitted")!=null&&request.getParameter("submitted").equals("true")?true:false;
		
		if(submitted){
			String email = request.getParameter("email");
			
			User user = userService.getByEmail(email);
			
			if(user!=null){
				try{
					
					Map<String, Object> params = new HashMap<String, Object>();
					params.put("password", user.getPassword());
					
					boolean mailSent = true;
					try{
						mailUtil.sendMessage(email, "Mappe - recupera password", pwdLostTemplate, params);
					}
					catch (Exception e) {
						mailSent = false;
						logger.error(e.getMessage());
					}
					
					mav.addObject("result", mailSent);
				}
				catch (Exception e) {
					logger.error(e.getMessage());
				}
			}
			else
				mav.addObject("result", false);
		}
				
		return mav;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public void setMailUtil(MailUtil mailUtil) {
		this.mailUtil = mailUtil;
	}

	public void setPwdLostTemplate(String pwdLostTemplate) {
		this.pwdLostTemplate = pwdLostTemplate;
	}
	
}

