package com.gwcworld.webapp.web.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.gwcworld.core.bean.ShapeType;
import com.gwcworld.core.service.ShapeTypeService;
import com.gwcworld.util.WebUtil;
import com.gwcworld.webapp.util.Constants;

public class ShapeTypes implements Controller{
			
	private ShapeTypeService shapeTypeService;
			
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {

		ModelAndView mav = null;
		
		List<ShapeType> shapeTypes = shapeTypeService.getAll();
		
		response.addHeader(Constants.CORS_HEADER, "*");
		
		WebUtil.respons2JsonArray(response, shapeTypes);
		
		return mav;
	}

	public void setShapeTypeService(ShapeTypeService shapeTypeService) {
		this.shapeTypeService = shapeTypeService;
	}
	
	
}
