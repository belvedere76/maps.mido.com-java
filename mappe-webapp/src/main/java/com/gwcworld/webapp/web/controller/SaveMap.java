package com.gwcworld.webapp.web.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.gwcworld.core.bean.Area;
import com.gwcworld.core.bean.AreaElement;
import com.gwcworld.core.bean.AreaElementType;
import com.gwcworld.core.bean.Exhibitor;
import com.gwcworld.core.bean.Layer;
import com.gwcworld.core.bean.Map;
import com.gwcworld.core.bean.Project;
import com.gwcworld.core.bean.Shape;
import com.gwcworld.core.bean.ShapeType;
import com.gwcworld.core.bean.Stand;
import com.gwcworld.core.bean.User;
import com.gwcworld.core.service.AreaElementTypeService;
import com.gwcworld.core.service.ExhibitorService;
import com.gwcworld.core.service.MapService;
import com.gwcworld.core.service.ProjectService;
import com.gwcworld.core.service.ShapeTypeService;
import com.gwcworld.core.service.StandStatusService;
import com.gwcworld.core.service.UserService;
import com.gwcworld.util.WebUtil;
import com.gwcworld.webapp.bean.AreaElementJson;
import com.gwcworld.webapp.bean.AreaJson;
import com.gwcworld.webapp.bean.LayerJson;
import com.gwcworld.webapp.bean.MapJson;
import com.gwcworld.webapp.bean.ShapeJson;
import com.gwcworld.webapp.bean.StandJson;
import com.gwcworld.webapp.util.Constants;


public class SaveMap implements Controller{
	
	private AreaElementTypeService areaElementTypeService;
	private ExhibitorService exhibitorService;
	private MapService mapService;
	private ProjectService projectService;
	private ShapeTypeService shapeTypeService;
	private StandStatusService standStatusService;
	private UserService userService;
	private Logger logger = LoggerFactory.getLogger(SaveMap.class);
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
	
		ModelAndView mav = null;
		
		String mapString = request.getParameter("map");
		int projectId = WebUtil.parameter2int(request.getParameter("projectId"));
		int userId = WebUtil.parameter2int(request.getParameter("userId"));
		
		JSONObject jsonObject = JSONObject.fromObject(mapString);
		HashMap classMap = new HashMap();  
		classMap.put( "areaList", AreaJson.class );
		classMap.put( "layerList", LayerJson.class);
		classMap.put( "areaElementList", AreaElementJson.class);
		classMap.put( "standList", StandJson.class);
		classMap.put( "shapeList", ShapeJson.class);
		
		MapJson mapJson = (MapJson)JSONObject.toBean(jsonObject, MapJson.class, classMap);
		
		logger.debug(mapJson.toString());
		
		int result = 1;
		if(mapJson!=null){
			
			try{
				Project project = projectService.getById(projectId);
				User user = userService.getById(userId);
				
				Map map = new Map();
				map.setName(mapJson.getName());
				map.setDescription(mapJson.getDescription());
				map.setMilestone(mapJson.isMilestone());
				map.setLastEdit(new Date());
				map.setProject(project);
				map.setUser(user);
				//mapService.save(map);
				
				List<AreaJson> areaJsonList = mapJson.getAreaList();
				List<Area> areaList = new ArrayList<Area>();
				for (Iterator<AreaJson> iterator = areaJsonList.iterator(); iterator.hasNext();) {
					AreaJson areaJson = iterator.next();
					
					Area area = new Area();
					area.setName(areaJson.getName());
					area.setGridsize(areaJson.getGridsize());
					area.setImgBackground(areaJson.getImgBackground());
					
					ShapeJson shapeJson = areaJson.getShapeJson();
					Shape shape = new Shape();
					shape.setColorRGBA(shapeJson.getColorRGBA());
					shape.setRotate(shapeJson.getRotate());
					shape.setScaleX(shapeJson.getScaleX());
					shape.setScaleY(shapeJson.getScaleY());
					ShapeType shapeType = shapeTypeService.getById(shapeJson.getShapeTypeId());
					shape.setType(shapeType);
					shape.setTranslateX(shapeJson.getTranslateX());
					shape.setTranslateY(shapeJson.getTranslateY());
					shape.setValues(shapeJson.getValues());
					//shapeService.save(shape);
					
					area.setShape(shape);
					
					List<LayerJson> layerJsonList = areaJson.getLayerList();
					List<Layer> layerList = new ArrayList<Layer>();
					for (Iterator<LayerJson> iterator2 = layerJsonList.iterator(); iterator2.hasNext();) {
						LayerJson layerJson = iterator2.next();
						
						Layer layer = new Layer();
						layer.setName(layerJson.getName());
						layer.setImgBackground(layerJson.getImgBackground());
						layer.setVisible(layerJson.isVisible());
						layer.setzIndex(layerJson.getzIndex());
						
						List<AreaElementJson> areaElementJsonList = layerJson.getAreaElementList();
						List<AreaElement> areaElementList = new ArrayList<AreaElement>();
						for (Iterator<AreaElementJson> iterator3 = areaElementJsonList.iterator(); iterator3.hasNext();) {
							AreaElementJson areaElementJson = iterator3.next();
							
							AreaElement areaElement = new AreaElement();
							AreaElementType areaElementType = areaElementTypeService.getById(areaElementJson.getAreaElementType());
							areaElement.setType(areaElementType);
							areaElement.setRotate(areaElementJson.getRotate());
							areaElement.setScaleX(areaElementJson.getScaleX());
							areaElement.setScaleY(areaElementJson.getScaleY());
							areaElement.setSurfaceTot(areaElementJson.getSurfaceTot());
							areaElement.setTranslateX(areaElementJson.getTranslateX());
							areaElement.setTranslateY(areaElementJson.getTranslateY());
							areaElement.setLayer(layer);
							
							areaElementList.add(areaElement);
						}
						layer.setAreaElements(areaElementList);
						
						List<ShapeJson> shapeJsonList = layerJson.getShapeList();
						List<Shape> shapeList = new ArrayList<Shape>();
						for (Iterator<ShapeJson> iterator4 = shapeJsonList.iterator(); iterator4.hasNext();) {
							ShapeJson layerShapeJson = iterator4.next();
							
							Shape layerShape = new Shape();
							layerShape.setColorRGBA(layerShapeJson.getColorRGBA());
							layerShape.setRotate(layerShapeJson.getRotate());
							layerShape.setScaleX(layerShapeJson.getScaleX());
							layerShape.setScaleY(layerShapeJson.getScaleY());
							ShapeType layerShapeType = shapeTypeService.getById(layerShapeJson.getShapeTypeId());
							layerShape.setType(layerShapeType);
							layerShape.setTranslateX(layerShapeJson.getTranslateX());
							layerShape.setTranslateY(layerShapeJson.getTranslateY());
							layerShape.setValues(layerShapeJson.getValues());
							
							shapeList.add(layerShape);
						}
						layer.setShapes(shapeList);
						
						List<StandJson> standJsonList = layerJson.getStandList();
						List<Stand> standList = new ArrayList<Stand>();
						for (Iterator<StandJson> iterator3 = standJsonList.iterator(); iterator3.hasNext();) {
							StandJson standJson = iterator3.next();
							
							Stand stand = new Stand();
							stand.setCode(standJson.getCode());
							stand.setDescription(standJson.getDescription());
							if(standJson.getExhibitorId()!=0){
								Exhibitor exhibitor = exhibitorService.getById(standJson.getExhibitorId());
								stand.setExhibitor(exhibitor);
							}
							stand.setLogo(standJson.getLogo());
							stand.setName(standJson.getName());
							stand.setOpenSides(standJson.getOpenSides());
							
							ShapeJson standShapeJson = standJson.getShape();
							Shape standShape = new Shape();
							standShape.setColorRGBA(standShapeJson.getColorRGBA());
							standShape.setRotate(standShapeJson.getRotate());
							standShape.setScaleX(standShapeJson.getScaleX());
							standShape.setScaleY(standShapeJson.getScaleY());
							ShapeType standShapeType = shapeTypeService.getById(standShapeJson.getShapeTypeId());
							standShape.setType(standShapeType);
							standShape.setTranslateX(standShapeJson.getTranslateX());
							standShape.setTranslateY(standShapeJson.getTranslateY());
							standShape.setValues(standShapeJson.getValues());
							stand.setShape(standShape);
							com.gwcworld.core.bean.StandStatus standStatus = standStatusService.getById(standJson.getStandStatusId());
							stand.setStatus(standStatus);
							stand.setSurfaceEffective(standJson.getSurfaceEffective());
							stand.setSurfaceTot(standJson.getSurfaceTot());
							stand.setOrientation(standJson.getOrientation());
							stand.setMoved(standJson.getMoved());
							stand.setLayer(layer);
							standList.add(stand);
						}
						layer.setStands(standList);
						layer.setArea(area);
						
						layerList.add(layer);
					}
					area.setLayers(layerList);
					area.setMap(map);
							
					areaList.add(area);
				}
				map.setAreaList(areaList);
				
				mapService.save(map);
				//List<Map> mapList = mapService.getMapsByProjectAndName(projectId, mapJson.getName(), 1);
				
				
				
			}
			catch(Exception e){
				result = 0;
				e.printStackTrace();
				logger.error(e.getMessage());
			}
			
		}
		
		response.addHeader(Constants.CORS_HEADER, "*");
		
		response.getWriter().print(result);
		
		return mav;
	}

	public void setAreaElementTypeService(
			AreaElementTypeService areaElementTypeService) {
		this.areaElementTypeService = areaElementTypeService;
	}

	public void setExhibitorService(ExhibitorService exhibitorService) {
		this.exhibitorService = exhibitorService;
	}

	public void setMapService(MapService mapService) {
		this.mapService = mapService;
	}

	public void setProjectService(ProjectService projectService) {
		this.projectService = projectService;
	}

	public void setShapeTypeService(ShapeTypeService shapeTypeService) {
		this.shapeTypeService = shapeTypeService;
	}

	public void setStandStatusService(StandStatusService standStatusService) {
		this.standStatusService = standStatusService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

}

