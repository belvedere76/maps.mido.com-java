package com.gwcworld.webapp.web.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.gwcworld.core.bean.User;
import com.gwcworld.core.service.UserService;
import com.gwcworld.webapp.util.Constants;

public class ConfirmUser implements Controller{
	
	private UserService userService;
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
	
		ModelAndView mav = new ModelAndView(Constants.CONFIRM_USER);
		
		String optInToken = request.getParameter("optInToken");
		if(optInToken!=null){
			User user = userService.getUserByToken(optInToken);
			
			boolean result = false;
			if(user!=null && !user.isActive()){
				user.setActivationDate(new Date());
				user.setActive(true);
				userService.save(user);
				result = true;
			}
			mav.addObject("result", result);
		}
		else{
			mav = null;
			response.sendRedirect("/it/login/");
		}
		return mav;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

}

