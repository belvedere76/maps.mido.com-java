package com.gwcworld.webapp.web.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.gwcworld.webapp.util.Constants;



public class Logout implements Controller{
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
	
		ModelAndView mav = null;
		
		request.getSession().removeAttribute(Constants.USER_SESSION);
		
		response.sendRedirect("/it/login/");
		
		return mav;
	}


}

