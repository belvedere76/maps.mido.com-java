package com.gwcworld.webapp.web;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.gwcworld.core.bean.User;
import com.gwcworld.webapp.util.Constants;


public class DispatcherServlet extends org.springframework.web.servlet.DispatcherServlet{
	
	public void doService(HttpServletRequest request, HttpServletResponse response) throws Exception{
		
		User user = (User)request.getSession().getAttribute(Constants.USER_SESSION);
		if((user==null || !user.isActive())
				&& request.getRequestURI().indexOf("addExhibitor")<0
				&& request.getRequestURI().indexOf("areaElementTypes")<0
				&& request.getRequestURI().indexOf("confirm")<0
				&& request.getRequestURI().indexOf("designerView")<0
				&& (request.getRequestURI().indexOf("exhibitorsJsonList.do")<0)
				&& (request.getRequestURI().indexOf("imageUpload.do")<0)
				&& request.getRequestURI().indexOf("loadMap")<0 
				&& request.getRequestURI().indexOf("login")<0 
				&& request.getRequestURI().indexOf("offersExport")<0 
				&& request.getRequestURI().indexOf("password")<0 
				&& request.getRequestURI().indexOf("saveMap")<0
				&& request.getRequestURI().indexOf("shapeTypes.do")<0
				&& request.getRequestURI().indexOf("standStatus.do")<0
				&& request.getRequestURI().indexOf("userGroups.do")<0
				)	
			response.sendRedirect("/it/login/");
		else
			super.doService(request, response);
		
	}
	
}

