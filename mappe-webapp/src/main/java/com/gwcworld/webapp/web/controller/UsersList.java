package com.gwcworld.webapp.web.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.gwcworld.core.bean.Customer;
import com.gwcworld.core.bean.User;
import com.gwcworld.core.service.CustomerService;
import com.gwcworld.core.service.UserService;
import com.gwcworld.util.WebUtil;
import com.gwcworld.webapp.util.Constants;

public class UsersList  implements Controller{
	
	private CustomerService customerService;
	private UserService userService;
			
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {

		ModelAndView mav = new ModelAndView(Constants.USERS_LIST);
		
		int customerId = WebUtil.parameter2int(request.getParameter("customerId"));
		String name = request.getParameter("name");
		
		Customer customer = customerService.getById(customerId);
		
		if(customer!=null){
			List<User> users = null;
			
			if(name==null || name.length()>1)
				users = userService.getUsersByCustomer(customer);
			else
				users = userService.getUsersByCustomerAndName(customer, name);
			
				mav.addObject("users", users);
				mav.addObject("name", name);
				mav.addObject("customer", customer);
		}
		else{
			mav = null;
			response.sendRedirect("/it/lista-progetti/");
		}
		return mav;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public void setCustomerService(CustomerService customerService) {
		this.customerService = customerService;
	}
	
}