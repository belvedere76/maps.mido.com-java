package com.gwcworld.webapp.web.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.gwcworld.core.bean.Customer;
import com.gwcworld.core.service.CustomerService;
import com.gwcworld.util.FileResources;
import com.gwcworld.util.FormRequest;
import com.gwcworld.util.FormRequestItem;
import com.gwcworld.util.WebUtil;
import com.gwcworld.webapp.bean.UploadResult;
import com.gwcworld.webapp.util.Constants;


public class ImageUpload implements Controller{
	
	private String fileUploadImgPath;
	private CustomerService customerService;
	private Logger logger = LoggerFactory.getLogger(ImageUpload.class);
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
	
		ModelAndView mav = null;
		
		UploadResult uploadResult = new UploadResult();
		uploadResult.setResult(1);
		
		FormRequest formrequest = new FormRequest(request);
		request.setAttribute("form_upload", formrequest);
		
		if(formrequest.getItem("customerId")!=null){
			
			int customerId = WebUtil.parameter2int(formrequest.getItem("customerId").getValue());
			
			Customer customer = customerService.getById(customerId);
			if(customer!=null){
				
				FormRequestItem imgFile = formrequest.getItem("img")!=null?formrequest.getItem("img"):null;
				String newFilename = "";
				try{
					newFilename = FileResources.saveFile(formrequest, imgFile, fileUploadImgPath+customer.getId()+"/");
				}
				catch(Exception e){
					logger.error(e.getMessage());
					uploadResult.setResult(0);
				}
				uploadResult.setFilename(newFilename);
			}
			else
				uploadResult.setResult(0);
		}
		else{
			uploadResult.setResult(0);
		}
		response.addHeader(Constants.CORS_HEADER, "*");
		
		WebUtil.respons2Json(response, uploadResult);
		
		return mav;
	}

	public void setFileUploadImgPath(String fileUploadImgPath) {
		this.fileUploadImgPath = fileUploadImgPath;
	}

	public void setCustomerService(CustomerService customerService) {
		this.customerService = customerService;
	}

}

