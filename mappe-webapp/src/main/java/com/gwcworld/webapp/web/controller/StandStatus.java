package com.gwcworld.webapp.web.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.gwcworld.core.service.StandStatusService;
import com.gwcworld.util.WebUtil;
import com.gwcworld.webapp.util.Constants;

public class StandStatus implements Controller{
			
	private StandStatusService standStatusService;
			
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {

		ModelAndView mav = null;
		
		List<com.gwcworld.core.bean.StandStatus> standStatus = standStatusService.getAll();
		
		response.addHeader(Constants.CORS_HEADER, "*");
		
		WebUtil.respons2JsonArray(response, standStatus);
		
		return mav;
	}

	public void setStandStatusService(StandStatusService standStatusService) {
		this.standStatusService = standStatusService;
	}
	
	
}
