package com.gwcworld.webapp.web.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.gwcworld.core.bean.Area;
import com.gwcworld.core.bean.AreaElement;
import com.gwcworld.core.bean.Layer;
import com.gwcworld.core.bean.Map;
import com.gwcworld.core.bean.Shape;
import com.gwcworld.core.bean.Stand;
import com.gwcworld.core.service.AreaElementService;
import com.gwcworld.core.service.AreaService;
import com.gwcworld.core.service.LayerService;
import com.gwcworld.core.service.MapService;
import com.gwcworld.core.service.StandService;
import com.gwcworld.util.WebUtil;
import com.gwcworld.webapp.bean.AreaElementJson;
import com.gwcworld.webapp.bean.AreaJson;
import com.gwcworld.webapp.bean.LayerJson;
import com.gwcworld.webapp.bean.MapJson;
import com.gwcworld.webapp.bean.ShapeJson;
import com.gwcworld.webapp.bean.StandJson;
import com.gwcworld.webapp.util.Constants;

public class LoadMap implements Controller{
	
	private AreaService areaService;
	private AreaElementService areaElementService;
	private LayerService layerService;
	private MapService mapService;
	private StandService standService;
	private Logger logger = LoggerFactory.getLogger(LoadMap.class);
			
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {

		ModelAndView mav = null;
		
		int idMap = WebUtil.parameter2int(request.getParameter("mapId"));
		
		Map map = mapService.getById(idMap);
		
		MapJson mapJson = new MapJson();
		mapJson.setName(map.getName());
		mapJson.setDescription(map.getDescription());
		mapJson.setMilestone(map.isMilestone());
		List<Area> areaList = areaService.getAreaListByMap(map);
		List<AreaJson> areaJsonList = new ArrayList<AreaJson>();
		for (Iterator<Area> iterator = areaList.iterator(); iterator.hasNext();) {
			Area area = iterator.next();
			
			AreaJson areaJson = new AreaJson();
			areaJson.setName(area.getName());
			areaJson.setGridsize(area.getGridsize());
			areaJson.setImgBackground(area.getImgBackground());
			Shape shape = area.getShape();
			ShapeJson shapeJson = new ShapeJson();
			shapeJson.setColorRGBA(shape.getColorRGBA());
			shapeJson.setRotate(shape.getRotate());
			shapeJson.setScaleX(shape.getScaleX());
			shapeJson.setScaleY(shape.getScaleY());
			shapeJson.setShapeTypeId(shape.getType().getId());
			shapeJson.setTranslateX(shape.getTranslateX());
			shapeJson.setTranslateY(shape.getTranslateY());
			shapeJson.setValues(shape.getValues());
			areaJson.setShapeJson(shapeJson);
			
			List<Layer> layerList = layerService.getLayerListByArea(area);
			List<LayerJson> layerJsonList = new ArrayList<LayerJson>();
			for (Iterator<Layer> iterator2 = layerList.iterator(); iterator2.hasNext();) {
				Layer layer = iterator2.next();
				LayerJson layerJson = new LayerJson();
				layerJson.setName(layer.getName());
				layerJson.setImgBackground(layer.getImgBackground());
				layerJson.setVisible(layer.isVisible());
				layerJson.setzIndex(layer.getzIndex());
				
				List<AreaElement> areaElementList = areaElementService.getAreaElementListByLayer(layer);
				List<AreaElementJson> areaElementJsonList = new ArrayList<AreaElementJson>();
				for (Iterator<AreaElement> iterator3 = areaElementList.iterator(); iterator3.hasNext();) {
					AreaElement areaElement = iterator3.next();
					AreaElementJson areaElementJson = new AreaElementJson();
					
					areaElementJson.setAreaElementType(areaElement.getType().getId());
					areaElementJson.setRotate(areaElement.getRotate());
					areaElementJson.setScaleX(areaElement.getScaleX());
					areaElementJson.setScaleY(areaElement.getScaleY());
					areaElementJson.setSurfaceTot(areaElement.getSurfaceTot());
					areaElementJson.setTranslateX(areaElement.getTranslateX());
					areaElementJson.setTranslateY(areaElement.getTranslateY());
					
					areaElementJsonList.add(areaElementJson);
				}
				layerJson.setAreaElementList(areaElementJsonList);
				
				List<Shape> shapeList = layer.getShapes();
				List<ShapeJson> shapeJsonList = new ArrayList<ShapeJson>();
				for (Iterator<Shape> iterator4 = shapeList.iterator(); iterator4.hasNext();) {
					Shape layerShape = iterator4.next();
					ShapeJson layerShapeJson = new ShapeJson();
					
					layerShapeJson.setColorRGBA(layerShape.getColorRGBA());
					layerShapeJson.setRotate(layerShape.getRotate());
					layerShapeJson.setScaleX(layerShape.getScaleX());
					layerShapeJson.setScaleY(layerShape.getScaleY());
					layerShapeJson.setShapeTypeId(layerShape.getType().getId());
					layerShapeJson.setTranslateX(layerShape.getTranslateX());
					layerShapeJson.setTranslateY(layerShape.getTranslateY());
					layerShapeJson.setValues(layerShape.getValues());
					
					shapeJsonList.add(layerShapeJson);
				}
				layerJson.setShapeList(shapeJsonList);
				
				List<Stand> standList = standService.getStandListByLayer(layer);
				List<StandJson> standJsonList = new ArrayList<StandJson>();
				for (Iterator<Stand> iterator3 = standList.iterator(); iterator3.hasNext();) {
					Stand stand = iterator3.next();
					StandJson standJson = new StandJson();
					standJson.setId(stand.getId());
					standJson.setCode(stand.getCode());
					standJson.setDescription(stand.getDescription());
					if(stand.getExhibitor()!=null)
						standJson.setExhibitorId(stand.getExhibitor().getId());
					standJson.setLogo(stand.getLogo());
					standJson.setName(stand.getName());
					standJson.setOpenSides(stand.getOpenSides());
					Shape standShape = stand.getShape();
					ShapeJson standShapeJson = new ShapeJson();
					standShapeJson.setColorRGBA(standShape.getColorRGBA());
					standShapeJson.setRotate(standShape.getRotate());
					standShapeJson.setScaleX(standShape.getScaleX());
					standShapeJson.setScaleY(standShape.getScaleY());
					standShapeJson.setShapeTypeId(standShape.getType().getId());
					standShapeJson.setTranslateX(standShape.getTranslateX());
					standShapeJson.setTranslateY(standShape.getTranslateY());
					standShapeJson.setValues(standShape.getValues());
					standJson.setShape(standShapeJson);
					standJson.setStandStatusId(stand.getStatus().getId());
					standJson.setSurfaceEffective(stand.getSurfaceEffective());
					standJson.setSurfaceTot(stand.getSurfaceTot());
					standJson.setOrientation(stand.getOrientation());
					standJson.setMoved(stand.getMoved());
					standJsonList.add(standJson);
				}
				layerJson.setStandList(standJsonList);
				
				layerJsonList.add(layerJson);
			}
			areaJson.setLayerList(layerJsonList);
					
			areaJsonList.add(areaJson);
		}		
		mapJson.setAreaList(areaJsonList);
		
		response.addHeader(Constants.CORS_HEADER, "*");
		
		WebUtil.respons2Json(response, mapJson);
		
		return mav;
	}

	public void setMapService(MapService mapService) {
		this.mapService = mapService;
	}

	public void setAreaService(AreaService areaService) {
		this.areaService = areaService;
	}

	public void setAreaElementService(AreaElementService areaElementService) {
		this.areaElementService = areaElementService;
	}

	public void setLayerService(LayerService layerService) {
		this.layerService = layerService;
	}

	public void setStandService(StandService standService) {
		this.standService = standService;
	}
	

}

