package com.gwcworld.webapp.web.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.gwcworld.webapp.util.Constants;

public class Header implements Controller{
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
	
		ModelAndView mav = new ModelAndView(Constants.HEADER);
		
		mav.addObject("isDesigner", request.getParameter("isDesigner"));
		mav.addObject("selectedMenu", request.getParameter("selectedMenu"));
  		mav.addObject("showUser", request.getParameter("showUser"));
  		mav.addObject("showProjects", request.getParameter("showProjects"));
  		mav.addObject("showExhibitors", request.getParameter("showExhibitors"));
				
		return mav;
	}


}

