package com.gwcworld.webapp.web.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.gwcworld.core.bean.User;
import com.gwcworld.webapp.util.Constants;

public class LeftColumn implements Controller{
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
	
		ModelAndView mav = new ModelAndView(Constants.LEFT_COLUMN);
		
		User user = (User)request.getSession().getAttribute(Constants.USER_SESSION);
		
		mav.addObject("customerId", request.getParameter("customerId"));
  		mav.addObject("selectedMenu", request.getParameter("selectedMenu"));
		mav.addObject("shortUsername", user.getEmail().substring(0, user.getEmail().indexOf("@")));
  		mav.addObject("showUser", request.getParameter("showUser"));
  		mav.addObject("showProjects", request.getParameter("showProjects"));
  		mav.addObject("showExhibitors", request.getParameter("showExhibitors"));
				
		return mav;
	}


}

