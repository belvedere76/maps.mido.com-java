package com.gwcworld.webapp.web.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.gwcworld.core.bean.Exhibitor;
import com.gwcworld.core.bean.Map;
import com.gwcworld.core.bean.Project;
import com.gwcworld.core.bean.Stand;
import com.gwcworld.core.service.ExhibitorService;
import com.gwcworld.core.service.MapService;
import com.gwcworld.core.service.ProjectService;
import com.gwcworld.core.service.StandService;
import com.gwcworld.util.WebUtil;
import com.gwcworld.webapp.util.Constants;

public class OffersExport implements Controller{
	
	private ExhibitorService exhibitorService;
	private MapService mapService;
	private ProjectService projectService;
	private StandService standService;
	private final int MAX_OFFERS = 3;
	private final int MAX_RESULT = 1;
	private final String TOKEN = "|#|";
			
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {

		ModelAndView mav = null;
		
		int projectId = WebUtil.parameter2int(request.getParameter("projectId"));
		
		Project project = projectService.getById(projectId);
		
		StringBuffer sb = new StringBuffer();
				
		if(project!=null){
			
			List<Exhibitor> exhibitors = exhibitorService.getExhibitorsByCustomer(project.getCustomer());
			
			List<Map[]> allMaps = mapService.getAllByProjectWithUniqueName(project, MAX_RESULT);
			List<Map> maps = new ArrayList<Map>();
			for (Map[] mapsArray : allMaps) {
				maps.add(mapsArray[0]);
			}
			
			for (Exhibitor exhibitor2 : exhibitors) {
				Exhibitor exhibitor = exhibitor2;
				//iddom, mq stand, alert variaz mq (s/n), nome padiglione, codice stand, link1, link 2, link 3
								
				int mq = 0;
				String mapName = " ";
				String standName = " ";
				String variazioneMq = "n";
				List<Stand> stands = standService.getStandListByExhibitor(maps, exhibitor);
				StringBuffer sbOffers = new StringBuffer();
				boolean addLink = true;
				int count = 1;
				for (Iterator<Stand> iterator = stands.iterator(); iterator.hasNext() && count <=MAX_OFFERS;) {
					Stand stand = iterator.next();
					if(addLink)
						sbOffers.append(stand.getLayer().getArea().getMap().getId()+"/"+stand.getId());
					else
						sbOffers.append(" ");
					sbOffers.append(TOKEN);
					
					if(stand.getStatus().getCode().equals(Constants.STAND_STATUS_CONFIRMED)){
						addLink = false;
						mq = stand.getSurfaceTot();
						mapName = stand.getLayer().getArea().getMap().getName();
						standName = stand.getCode();
						variazioneMq = exhibitor.getMqRequested()!=mq?"s":"n";
					}
					count++;
				}
				
				int padding = MAX_OFFERS-stands.size();
				for (int i = 0; i < padding; i++) {
					sbOffers.append(" "+TOKEN);
				}
				
				// ID domanda
				sb.append(exhibitor.getId3rdParty()+TOKEN);
				// Mq totali in caso di stand confermato
				sb.append(mq+TOKEN);
				// Variazione Mq richiesti da Mq totali per stand confermato (s/n)
				sb.append(variazioneMq+TOKEN);
				// Nome mappa per eventuale stand confermato
				sb.append(mapName+TOKEN);
				// Nome stand per eventuale stand confermato
				sb.append(standName+TOKEN);
				// Link offerte
				sb.append(sbOffers.toString());
				sb.append("\n");
			}
		}
		sb.append("EOF");
		
		response.getWriter().print(sb.toString());
				
		return mav;
	}

	public void setExhibitorService(ExhibitorService exhibitorService) {
		this.exhibitorService = exhibitorService;
	}

	public void setMapService(MapService mapService) {
		this.mapService = mapService;
	}

	public void setProjectService(ProjectService projectService) {
		this.projectService = projectService;
	}

	public void setStandService(StandService standService) {
		this.standService = standService;
	}

}
