package com.gwcworld.webapp.web.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.gwcworld.core.bean.Customer;
import com.gwcworld.core.bean.Exhibitor;
import com.gwcworld.core.service.CustomerService;
import com.gwcworld.core.service.ExhibitorService;
import com.gwcworld.util.WebUtil;
import com.gwcworld.webapp.util.Constants;

public class ExhibitorsList implements Controller{
	
	private CustomerService customerService;
	private ExhibitorService exhibitorService;
			
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {

		ModelAndView mav = new ModelAndView(Constants.EXHIBITORS_LIST);
		
		int customerId = WebUtil.parameter2int(request.getParameter("customerId"));
		String name = request.getParameter("name");
		
		Customer customer = customerService.getById(customerId);
		
		if(customer!=null){
			List<Exhibitor> exhibitors = null;
			
			if(name==null || name.length()>1)
				exhibitors = exhibitorService.getExhibitorsByCustomer(customer);
			else
				exhibitors = exhibitorService.getExhibitorsByCustomerAndName(customer, name);
			
			mav.addObject("exhibitors", exhibitors);
			mav.addObject("name", name);
			mav.addObject("customer", customer);
		}
		else{
			mav = null;
			response.sendRedirect("/it/lista-progetti/");
		}
		return mav;
	}

	public void setExhibitorService(ExhibitorService exhibitorService) {
		this.exhibitorService = exhibitorService;
	}

	public void setCustomerService(CustomerService customerService) {
		this.customerService = customerService;
	}
	
}

