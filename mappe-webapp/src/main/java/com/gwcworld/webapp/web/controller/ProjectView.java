package com.gwcworld.webapp.web.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.gwcworld.core.bean.Map;
import com.gwcworld.core.bean.Project;
import com.gwcworld.core.bean.User;
import com.gwcworld.core.service.MapService;
import com.gwcworld.core.service.ProjectService;
import com.gwcworld.util.WebUtil;
import com.gwcworld.webapp.util.Constants;

public class ProjectView implements Controller{
	
	private MapService mapService;
	private ProjectService projectService;
	private final int MAX_RESULT = 5;
	private Logger logger = LoggerFactory.getLogger(ProjectView.class);

	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
	
		ModelAndView mav = new ModelAndView(Constants.PROJECT_VIEW);
		
		int projectId = WebUtil.parameter2int(request.getParameter("projectId"));
		User user = (User)request.getSession().getAttribute(Constants.USER_SESSION);

		Project project = projectService.getById(projectId);
		Date lastSavedMap = mapService.getLastSaveDate(project);
		project.setLastEdit(lastSavedMap);
		
		if(project!=null && (user.getUserGroup().getLevel()==Constants.USER_GROUP_SUPERUSER || user.getCustomer().getId()==project.getCustomer().getId())){
			
			List<Map[]> mapsByProjectAndUniqueName = mapService.getAllByProjectWithUniqueName(project, MAX_RESULT);
			
			request.getSession().setAttribute(Constants.PROJECT_ID_FOR_EXHIBITOR_VIEW, new Integer(projectId));
			mav.addObject("allMapsByName", mapsByProjectAndUniqueName);
			mav.addObject("customerId", project.getCustomer().getId());
			mav.addObject("project", project);
		}
		else{
			mav = null;
			response.sendRedirect("/it/lista-progetti/");
		}
		return mav;
	}

	public void setMapService(MapService mapService) {
		this.mapService = mapService;
	}

	public void setProjectService(ProjectService projectService) {
		this.projectService = projectService;
	}
	
}

