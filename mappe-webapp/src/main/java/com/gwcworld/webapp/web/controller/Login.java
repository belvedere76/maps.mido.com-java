package com.gwcworld.webapp.web.controller;

import java.util.Date;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.gwcworld.core.bean.User;
import com.gwcworld.core.service.UserService;
import com.gwcworld.util.WebUtil;
import com.gwcworld.webapp.util.Constants;

public class Login implements Controller{
			
	private UserService userService;
			
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {

		ModelAndView mav = new ModelAndView(Constants.LOGIN);
		
		boolean submitted = WebUtil.parameter2boolean(request.getParameter("submitted"));
		
		if(submitted){
			String email = request.getParameter("email");
			String password = request.getParameter("password");
			boolean rememberMe = WebUtil.parameter2boolean(request.getParameter("rememberMe"));
			
			User user = userService.login(email, password);
			
			if(user!=null){
				
				userService.updateUserAccesTime(user, new Date());
				
				if(rememberMe){
					Cookie rememberMeCookie = new Cookie(Constants.USER_LOGIN_COOKIE, user.getEmail());
					rememberMeCookie.setDomain(Constants.SSO_REALM);
					rememberMeCookie.setMaxAge(60*60*24*30);
					rememberMeCookie.setValue(user.getEmail());
					response.addCookie(rememberMeCookie);
				}
				request.getSession().setAttribute(Constants.USER_SESSION, user);
				//mav = new ModelAndView("redirect:/it/lista-progetti/");
				mav = null;
				response.sendRedirect("/it/lista-progetti/");
			}
			
		}
		else{
			String lastEmailLogin = null;
			
			Cookie[] cookies = request.getCookies();
			if(cookies!=null){
				for (int i = 0; i < cookies.length; i++) {
					Cookie cookie = cookies[i];
					if(cookie.getName().equals(Constants.USER_LOGIN_COOKIE))
						lastEmailLogin = cookie.getValue();
				}
				mav.addObject("lastEmailLogin", lastEmailLogin);
			}
		}
				
		return mav;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}	
	
}

