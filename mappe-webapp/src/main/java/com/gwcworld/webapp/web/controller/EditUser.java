package com.gwcworld.webapp.web.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.gwcworld.core.bean.Customer;
import com.gwcworld.core.bean.User;
import com.gwcworld.core.bean.UserGroup;
import com.gwcworld.core.service.CustomerService;
import com.gwcworld.core.service.UserGroupService;
import com.gwcworld.core.service.UserService;
import com.gwcworld.util.MailUtil;
import com.gwcworld.util.Md5Util;
import com.gwcworld.util.WebUtil;
import com.gwcworld.webapp.util.Constants;

public class EditUser implements Controller{
	
	private CustomerService customerService;
	private String emailConfirmTemplate;
	private MailUtil mailUtil;
	private UserGroupService userGroupService;
	private UserService userService;
	private Logger logger = LoggerFactory.getLogger(EditUser.class);
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
	
		ModelAndView mav = new ModelAndView(Constants.EDIT_USER);
		
		User user = (User)request.getSession().getAttribute(Constants.USER_SESSION);
		boolean submitted = WebUtil.parameter2boolean(request.getParameter("submitted"));
		
		List<Customer> customers = customerService.getAll();
		List<UserGroup> groups = userGroupService.getAll();
		
		int userId = WebUtil.parameter2int(request.getParameter("userId"));
		User userView = userService.getById(userId);
		logger.debug("1");
		if((userView == null && (user.getUserGroup().getLevel2int()==Constants.USER_GROUP_SUPERUSER || user.getUserGroup().getLevel2int()==Constants.USER_GROUP_ADMIN)) 
			|| (userView!=null && (user.getUserGroup().getLevel2int()==Constants.USER_GROUP_SUPERUSER || (user.getUserGroup().getLevel2int()==Constants.USER_GROUP_ADMIN && user.getCustomer().equals(userView.getCustomer()))))
			||  (userView!=null && userView.getId() == userView.getId())){
			logger.debug("2");
			if(submitted){
				logger.debug("3");
				String userEnabled = request.getParameter("userEnabled");
				int customerId = WebUtil.parameter2int(request.getParameter("customerId"));
				int groupId = WebUtil.parameter2int(request.getParameter("groupId"));
				
				Customer customer = customerService.getById(customerId);
				UserGroup userGroup = userGroupService.getById(groupId);
				
				boolean result = true;
				
				if(userId!=0 && userView!=null){
					logger.debug("4");
					if(userEnabled!=null && user.getId()!=userId)
						userView.setActive(userEnabled.equals(Constants.USER_ACTIVE)?true:false);
					if(customer!=null)
						userView.setCustomer(customer);
					if(userGroup!=null)
						userView.setUserGroup(userGroup);
					userService.save(userView);
					
					mav = new ModelAndView(Constants.VIEW_USER);
				}
				else{
					logger.debug("5");
					String email = request.getParameter("email");
					String password = request.getParameter("password");
					
					if(userService.getByEmail(email)!=null){
						result = false;
					}
					else{
						logger.debug("6");
						User newUser = new User();
						newUser.setEmail(email);
						newUser.setPassword(password);
						
						newUser.setCustomer(customer);
						newUser.setUserGroup(userGroup);
						
						Date now = new Date();
						SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy/HH:mm");
						newUser.setRegistrationDate(now);
						
						newUser.setOptInToken(Md5Util.encript(sdf.format(now)+email));
						
						try{
							
							Map<String, Object> params = new HashMap<String, Object>();
							params.put("email", newUser.getEmail());
							params.put("password", newUser.getPassword());
							params.put("doubleOptIn", newUser.getOptInToken());
							
							try{
								mailUtil.sendMessage(email, "Mappe - conferma registrazione", emailConfirmTemplate, params);
							}
							catch (Exception e) {
								result = false;
								logger.error(e.getMessage());
							}
							
						}
						catch (Exception e) {
							logger.error(e.getMessage());
						}
						
						if(result)
							userService.save(newUser);
						
					}
				}
				mav.addObject("result", result);
			}
			else{
				if(userId!=0 && userView!=null)
					mav = new ModelAndView(Constants.VIEW_USER);
				
			}
			
			mav.addObject("userView", userView);
			
			mav.addObject("userMenu", userView!=null&&userView.getId()==user.getId()?"viewUser":"editUser");
			mav.addObject("customers", customers);
			mav.addObject("groups", groups);
			
		}
		else{
			mav = null;
			response.sendRedirect("/it/lista-progetti/");
		}
		
		return mav;
	}

	public void setCustomerService(CustomerService customerService) {
		this.customerService = customerService;
	}

	public void setUserGroupService(UserGroupService userGroupService) {
		this.userGroupService = userGroupService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public void setEmailConfirmTemplate(String emailConfirmTemplate) {
		this.emailConfirmTemplate = emailConfirmTemplate;
	}

	public void setMailUtil(MailUtil mailUtil) {
		this.mailUtil = mailUtil;
	}

}

