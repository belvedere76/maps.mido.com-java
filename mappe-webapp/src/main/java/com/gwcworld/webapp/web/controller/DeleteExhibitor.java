package com.gwcworld.webapp.web.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.gwcworld.core.bean.Exhibitor;
import com.gwcworld.core.service.ExhibitorService;
import com.gwcworld.util.WebUtil;
import com.gwcworld.webapp.util.Constants;

public class DeleteExhibitor implements Controller{
	
	private ExhibitorService exhibitorService;
	private Logger logger = LoggerFactory.getLogger(DeleteExhibitor.class);
			
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {

		ModelAndView mav = null;
		
		int customerId = WebUtil.parameter2int(request.getParameter("customerId"));
		int idExhibitor = WebUtil.parameter2int(request.getParameter("idExhibitor"));
		
		Exhibitor exhibitor = exhibitorService.getById(idExhibitor);
				
		if(exhibitor!=null){
			try{
				exhibitorService.delete(exhibitor);
			}
			catch(Exception e){
				logger.warn("Espositore "+idExhibitor+" NON cancellato: "+e);
			}
		}
		
		response.sendRedirect("/it/lista-espositori/"+customerId+"/"+Constants.ALL_PARAM);
			
		return mav;
	}

	public void setExhibitorService(ExhibitorService exhibitorService) {
		this.exhibitorService = exhibitorService;
	}
	
}

