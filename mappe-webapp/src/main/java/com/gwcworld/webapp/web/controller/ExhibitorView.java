package com.gwcworld.webapp.web.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.gwcworld.core.bean.Exhibitor;
import com.gwcworld.core.bean.Map;
import com.gwcworld.core.bean.Project;
import com.gwcworld.core.bean.Stand;
import com.gwcworld.core.service.ExhibitorService;
import com.gwcworld.core.service.MapService;
import com.gwcworld.core.service.ProjectService;
import com.gwcworld.core.service.StandService;
import com.gwcworld.core.service.StandStatusService;
import com.gwcworld.util.WebUtil;
import com.gwcworld.webapp.util.Constants;

public class ExhibitorView implements Controller{
	
	private ExhibitorService exhibitorService;
	private MapService mapService;
	private ProjectService projectService;
	private StandService standService;
	private StandStatusService standStatusService;
	private final int MAX_RESULT = 1;
			
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {

		ModelAndView mav = new ModelAndView(Constants.EXHIBITORS_VIEW);
		
		int idExhibitor = WebUtil.parameter2int(request.getParameter("idExhibitor"));
		int idProject = (Integer)request.getSession().getAttribute(Constants.PROJECT_ID_FOR_EXHIBITOR_VIEW);
		
		Exhibitor exhibitor = exhibitorService.getById(idExhibitor);
		Project project = projectService.getById(idProject);
				
		if(exhibitor!=null && project!=null){
			
			List<Map[]> allMaps = mapService.getAllByProjectWithUniqueName(project, MAX_RESULT);
			List<Map> maps = new ArrayList<Map>();
			for (Map[] mapsArray : allMaps) {
				maps.add(mapsArray[0]);
			}
			
			List<Stand> stands = standService.getStandListByExhibitor(maps, exhibitor);
			List<com.gwcworld.core.bean.StandStatus> standStatusList = standStatusService.getAll();
			
			mav.addObject("exhibitor", exhibitor);
			mav.addObject("customerId", exhibitor.getCustomer().getId());
			mav.addObject("project", project);
			mav.addObject("stands", stands);
			mav.addObject("standStatusList", standStatusList);
		}
		else
			mav = new ModelAndView(Constants.EXHIBITORS_LIST);
				
		return mav;
	}

	public void setExhibitorService(ExhibitorService exhibitorService) {
		this.exhibitorService = exhibitorService;
	}

	public void setMapService(MapService mapService) {
		this.mapService = mapService;
	}

	public void setProjectService(ProjectService projectService) {
		this.projectService = projectService;
	}

	public void setStandService(StandService standService) {
		this.standService = standService;
	}

	public void setStandStatusService(StandStatusService standStatusService) {
		this.standStatusService = standStatusService;
	}

	
}