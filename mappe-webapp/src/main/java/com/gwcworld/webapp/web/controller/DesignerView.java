package com.gwcworld.webapp.web.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.gwcworld.core.bean.Map;
import com.gwcworld.core.bean.Stand;
import com.gwcworld.core.service.MapService;
import com.gwcworld.core.service.StandService;
import com.gwcworld.util.WebUtil;
import com.gwcworld.webapp.util.Constants;

public class DesignerView implements Controller{
	
	private MapService mapService;
	private StandService standService;
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
	
		ModelAndView mav = new ModelAndView(Constants.DESIGNER_READONLY);
		
		int idMap = WebUtil.parameter2int(request.getParameter("idMap"));
		int idstand = WebUtil.parameter2int(request.getParameter("idStand"));
		
		if(idMap!=0){
			Map map = mapService.getById(idMap);
			mav.addObject("map", map);
		}
		
		if(idstand!=0){
			Stand stand = standService.getById(idstand);
			mav.addObject("stand", stand);
		}
		
		return mav;
	}

	public void setMapService(MapService mapService) {
		this.mapService = mapService;
	}

	public void setStandService(StandService standService) {
		this.standService = standService;
	}

}

