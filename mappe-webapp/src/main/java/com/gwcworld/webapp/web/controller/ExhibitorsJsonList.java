package com.gwcworld.webapp.web.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.gwcworld.core.bean.Exhibitor;
import com.gwcworld.core.bean.Project;
import com.gwcworld.core.service.ExhibitorService;
import com.gwcworld.core.service.ProjectService;
import com.gwcworld.util.WebUtil;
import com.gwcworld.webapp.util.Constants;

public class ExhibitorsJsonList implements Controller{
	
	private ExhibitorService exhibitorService;
	private ProjectService projectService;
			
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {

		ModelAndView mav = null;
		
		int projectId = WebUtil.parameter2int(request.getParameter("projectId"));
		String name = request.getParameter("name");
		
		Project project = projectService.getById(projectId);
		
		List<Exhibitor> exhibitors = new ArrayList<Exhibitor>();
			
		if(name==null || name.equalsIgnoreCase(Constants.ALL_PARAM))
			exhibitors = exhibitorService.getExhibitorsByCustomer(project.getCustomer());
		else
			exhibitors = exhibitorService.getExhibitorsByCustomerAndName(project.getCustomer(), name);
		
		List<Exhibitor> exhibitorsList = new ArrayList<Exhibitor>();
		if(exhibitors!=null){
			for (Iterator<Exhibitor> iterator = exhibitors.iterator(); iterator.hasNext();) {
				Exhibitor exhibitor = iterator.next();
				exhibitor.setCustomer(null);
				exhibitorsList.add(exhibitor);
			}
		}
		
		response.addHeader(Constants.CORS_HEADER, "*");
		
		WebUtil.respons2JsonArray(response, exhibitorsList);
		
		return mav;
	}

	public void setExhibitorService(ExhibitorService exhibitorService) {
		this.exhibitorService = exhibitorService;
	}

	public void setProjectService(ProjectService projectService) {
		this.projectService = projectService;
	}
	
}

