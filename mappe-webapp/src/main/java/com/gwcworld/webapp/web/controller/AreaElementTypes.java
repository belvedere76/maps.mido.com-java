package com.gwcworld.webapp.web.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.gwcworld.core.bean.AreaElementType;
import com.gwcworld.core.service.AreaElementTypeService;
import com.gwcworld.util.WebUtil;
import com.gwcworld.webapp.util.Constants;

public class AreaElementTypes implements Controller{
			
	private AreaElementTypeService areaElementTypeService;
			
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {

		ModelAndView mav = null;
		
		List<AreaElementType> areaElementTypes = areaElementTypeService.getAll();
		
		response.addHeader(Constants.CORS_HEADER, "*");
		
		WebUtil.respons2JsonArray(response, areaElementTypes);
		
		return mav;
	}

	public void setAreaElementTypeService(AreaElementTypeService areaElementTypeService) {
		this.areaElementTypeService = areaElementTypeService;
	}
	
	
}