package com.gwcworld.webapp.web.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.gwcworld.core.bean.Customer;
import com.gwcworld.core.bean.Exhibitor;
import com.gwcworld.core.service.CustomerService;
import com.gwcworld.core.service.ExhibitorService;
import com.gwcworld.util.WebUtil;
import com.gwcworld.webapp.util.Constants;

public class AddExhibitor implements Controller{
	
	private CustomerService customerService;
	private ExhibitorService exhibitorService;
			
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {

		ModelAndView mav = null;
		
		String id3rdParty = request.getParameter("id");
		String lastId3rdParty = request.getParameter("id_last_year");
		String name = request.getParameter("name");
		String address = request.getParameter("address");
		String tel = request.getParameter("tel");
		String fax = request.getParameter("fax");
		String email = request.getParameter("email");
		int mqRequested = WebUtil.parameter2int(request.getParameter("mqRequested"));
		boolean changeRequest = WebUtil.parameter2boolean(request.getParameter("changeRequest"));
		String token3rdParty = request.getParameter("token");
		
		Customer customer = customerService.getCustomerByToken(token3rdParty);
		
		int result = 0;
		
		if(customer!=null){
			Exhibitor exhibitor = exhibitorService.getExhibitorsByCustomerAndExactName(customer, name);
			if(exhibitor==null)
				exhibitor = new Exhibitor();
			
			exhibitor.setId3rdParty(id3rdParty);
			exhibitor.setLastId3rdParty(lastId3rdParty);
			exhibitor.setName(name);
			exhibitor.setAddress(address);
			exhibitor.setTel(tel);
			exhibitor.setFax(fax);
			exhibitor.setEmail(email);
			exhibitor.setMqRequested(mqRequested);
			exhibitor.setChangeRequest(changeRequest);
			exhibitor.setCustomer(customer);
			
			exhibitorService.save(exhibitor);
			
			result = 1;
		}
		
		response.addHeader(Constants.CORS_HEADER, "*");
		
		response.getWriter().write(String.valueOf(result));
		
		return mav;
	}

	public void setExhibitorService(ExhibitorService exhibitorService) {
		this.exhibitorService = exhibitorService;
	}

	public void setCustomerService(CustomerService customerService) {
		this.customerService = customerService;
	}
	
}

