package com.gwcworld.webapp.web.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.gwcworld.core.bean.Map;
import com.gwcworld.core.bean.Project;
import com.gwcworld.core.service.MapService;
import com.gwcworld.core.service.ProjectService;
import com.gwcworld.util.WebUtil;
import com.gwcworld.webapp.util.Constants;

public class Designer implements Controller{
	
	private MapService mapService;
	private ProjectService projectService;
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
	
		ModelAndView mav = new ModelAndView(Constants.DESIGNER);
		
		int idMap = WebUtil.parameter2int(request.getParameter("idMap"));
		int idProject = WebUtil.parameter2int(request.getParameter("idProject"));
		
		if(idMap!=0){
			Map map = mapService.getById(idMap);
			mav.addObject("map", map);
		}
		
		if(idProject!=0){
			Project project = projectService.getById(idProject);
			mav.addObject("project", project);
		}
		
		return mav;
	}

	public void setMapService(MapService mapService) {
		this.mapService = mapService;
	}

	public void setProjectService(ProjectService projectService) {
		this.projectService = projectService;
	}

}

