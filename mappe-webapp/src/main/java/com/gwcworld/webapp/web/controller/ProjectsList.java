package com.gwcworld.webapp.web.controller;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.gwcworld.core.bean.Project;
import com.gwcworld.core.bean.User;
import com.gwcworld.core.service.MapService;
import com.gwcworld.core.service.ProjectService;
import com.gwcworld.webapp.util.Constants;


public class ProjectsList implements Controller{
	
	private MapService mapService;
	private ProjectService projectService;
			
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {

		ModelAndView mav = new ModelAndView(Constants.PROJECT_LIST);
		
		User user = (User)request.getSession().getAttribute(Constants.USER_SESSION);
		
		if(request.getSession().getAttribute(Constants.PROJECT_ID_FOR_EXHIBITOR_VIEW)!=null)
			request.getSession().removeAttribute(Constants.PROJECT_ID_FOR_EXHIBITOR_VIEW);
			
		List<Project> projects = null;
		
		if(user.getUserGroup().getLevel()==1)
			projects = projectService.getAll();
		else
			projects = projectService.getProjectsByCustomer(user.getCustomer());
		
		if(projects!=null){
			for (Iterator<Project> iterator = projects.iterator(); iterator.hasNext();) {
				Project project = iterator.next();
				Date lastSavedMap = mapService.getLastSaveDate(project);
				project.setLastEdit(lastSavedMap);
			}
		}
		
		mav.addObject("projects", projects);
		
		return mav;
	}

	public void setMapService(MapService mapService) {
		this.mapService = mapService;
	}

	public void setProjectService(ProjectService projectService) {
		this.projectService = projectService;
	}
	
}

