package com.gwcworld.webapp.bean;


public class ShapeJson {

	private String values;
	private int translateX;
	private int translateY;
	private float scaleX;
	private float scaleY;
	private float rotate;
	private String colorRGBA;
	private int shapeTypeId;
	
	
	public String getValues() {
		return values;
	}
	public void setValues(String values) {
		this.values = values;
	}
	public int getTranslateX() {
		return translateX;
	}
	public void setTranslateX(int translateX) {
		this.translateX = translateX;
	}
	public int getTranslateY() {
		return translateY;
	}
	public void setTranslateY(int translateY) {
		this.translateY = translateY;
	}
	public float getScaleX() {
		return scaleX;
	}
	public void setScaleX(float scaleX) {
		this.scaleX = scaleX;
	}
	public float getScaleY() {
		return scaleY;
	}
	public void setScaleY(float scaleY) {
		this.scaleY = scaleY;
	}
	public float getRotate() {
		return rotate;
	}
	public void setRotate(float rotate) {
		this.rotate = rotate;
	}
	public String getColorRGBA() {
		return colorRGBA;
	}
	public void setColorRGBA(String colorRGBA) {
		this.colorRGBA = colorRGBA;
	}
	public int getShapeTypeId() {
		return shapeTypeId;
	}
	public void setShapeTypeId(int shapeTypeId) {
		this.shapeTypeId = shapeTypeId;
	}
	
}
