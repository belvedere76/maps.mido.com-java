package com.gwcworld.webapp.bean;

import java.util.ArrayList;
import java.util.List;


public class AreaJson {
	
	private String name;
	private String gridsize;
	private String imgBackground;
	private ShapeJson shapeJson;

	public List<LayerJson> layerList = new ArrayList<LayerJson>();

	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGridsize() {
		return gridsize;
	}
	public void setGridsize(String gridsize) {
		this.gridsize = gridsize;
	}
	public String getImgBackground() {
		return imgBackground;
	}
	public void setImgBackground(String imgBackground) {
		this.imgBackground = imgBackground;
	}
	public ShapeJson getShapeJson() {
		return shapeJson;
	}
	public void setShapeJson(ShapeJson shapeJson) {
		this.shapeJson = shapeJson;
	}
	public List<LayerJson> getLayerList() {
		return layerList;
	}
	public void setLayerList(List<LayerJson> layerList) {
		this.layerList = layerList;
	}
	
}
