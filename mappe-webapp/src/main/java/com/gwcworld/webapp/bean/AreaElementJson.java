package com.gwcworld.webapp.bean;


public class AreaElementJson {
	
	private short surfaceTot;
	private int translateX;
	private int translateY;
	private float scaleX;
	private float scaleY;
	private float rotate;
	private int areaElementType;
	
	public short getSurfaceTot() {
		return surfaceTot;
	}
	public void setSurfaceTot(short surfaceTot) {
		this.surfaceTot = surfaceTot;
	}
	public int getTranslateX() {
		return translateX;
	}
	public void setTranslateX(int translateX) {
		this.translateX = translateX;
	}
	public int getTranslateY() {
		return translateY;
	}
	public void setTranslateY(int translateY) {
		this.translateY = translateY;
	}
	public float getScaleX() {
		return scaleX;
	}
	public void setScaleX(float scaleX) {
		this.scaleX = scaleX;
	}
	public float getScaleY() {
		return scaleY;
	}
	public void setScaleY(float scaleY) {
		this.scaleY = scaleY;
	}
	public float getRotate() {
		return rotate;
	}
	public void setRotate(float rotate) {
		this.rotate = rotate;
	}
	public int getAreaElementType() {
		return areaElementType;
	}
	public void setAreaElementType(int areaElementType) {
		this.areaElementType = areaElementType;
	}

}
