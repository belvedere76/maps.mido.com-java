package com.gwcworld.webapp.bean;


public class StandJson {
	
	private int id;
	private String name;
	private String code;
	private short surfaceTot;
	private short surfaceEffective;
	private String orientation;
	private boolean moved;
	private short openSides;
	private String description;
	private String logo;
	private int exhibitorId;
	private int standStatusId;
	private ShapeJson shape;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public short getSurfaceTot() {
		return surfaceTot;
	}
	public void setSurfaceTot(short surfaceTot) {
		this.surfaceTot = surfaceTot;
	}
	public short getSurfaceEffective() {
		return surfaceEffective;
	}
	public void setSurfaceEffective(short surfaceEffective) {
		this.surfaceEffective = surfaceEffective;
	}
	public String getOrientation() {
		return orientation;
	}
	public void setOrientation(String orientation) {
		this.orientation = orientation;
	}
	public boolean getMoved() {
		return moved;
	}
	public void setMoved(boolean moved) {
		this.moved = moved;
	}
	public short getOpenSides() {
		return openSides;
	}
	public void setOpenSides(short openSides) {
		this.openSides = openSides;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getLogo() {
		return logo;
	}
	public void setLogo(String logo) {
		this.logo = logo;
	}
	public int getExhibitorId() {
		return exhibitorId;
	}
	public void setExhibitorId(int exhibitorId) {
		this.exhibitorId = exhibitorId;
	}
	public int getStandStatusId() {
		return standStatusId;
	}
	public void setStandStatusId(int standStatusId) {
		this.standStatusId = standStatusId;
	}
	public ShapeJson getShape() {
		return shape;
	}
	public void setShape(ShapeJson shape) {
		this.shape = shape;
	}

}
