package com.gwcworld.webapp.bean;

public class UploadResult {
	
	private int result;
	private String filename;
	
	
	public int getResult() {
		return result;
	}
	public void setResult(int result) {
		this.result = result;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}	

}
