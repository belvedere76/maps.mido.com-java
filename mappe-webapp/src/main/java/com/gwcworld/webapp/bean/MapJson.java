package com.gwcworld.webapp.bean;

import java.util.ArrayList;
import java.util.List;


public class MapJson {
	
	private String name;
	private String description;
	private boolean milestone;
	
	private List<AreaJson> areaList = new ArrayList<AreaJson>();
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public boolean isMilestone() {
		return milestone;
	}
	public void setMilestone(boolean milestone) {
		this.milestone = milestone;
	}
	public List<AreaJson> getAreaList() {
		return areaList;
	}
	public void setAreaList(List<AreaJson> areaList) {
		this.areaList = areaList;
	}
	@Override
	public String toString() {
		return "MapJson [name=" + name + ", description=" + description
				+ ", milestone=" + milestone + ", areaList=" + areaList + "]";
	}

}
