package com.gwcworld.webapp.bean;

import java.util.ArrayList;
import java.util.List;

public class LayerJson {
	
	private String name;
	private byte zIndex;
	private String imgBackground;
	private boolean visible;
	private List<AreaElementJson> areaElementList = new ArrayList<AreaElementJson>();
	private List<StandJson> standList = new ArrayList<StandJson>();
	private List<ShapeJson> shapeList = new ArrayList<ShapeJson>();

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public byte getzIndex() {
		return zIndex;
	}
	public void setzIndex(byte zIndex) {
		this.zIndex = zIndex;
	}
	public String getImgBackground() {
		return imgBackground;
	}
	public void setImgBackground(String imgBackground) {
		this.imgBackground = imgBackground;
	}
	public boolean isVisible() {
		return visible;
	}
	public void setVisible(boolean visible) {
		this.visible = visible;
	}
	public List<AreaElementJson> getAreaElementList() {
		return areaElementList;
	}
	public void setAreaElementList(List<AreaElementJson> areaElementList) {
		this.areaElementList = areaElementList;
	}
	public List<StandJson> getStandList() {
		return standList;
	}
	public void setStandList(List<StandJson> standList) {
		this.standList = standList;
	}
	public List<ShapeJson> getShapeList() {
		return shapeList;
	}
	public void setShapeList(List<ShapeJson> shapeList) {
		this.shapeList = shapeList;
	}

}
