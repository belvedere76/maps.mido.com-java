package com.gwcworld.webapp.util;

public class Constants {
	
	public static final String CONFIRM_USER = "attivazione_user";
	public static final String DESIGNER = "designer";
	public static final String DESIGNER_READONLY = "designer-read-only";
	public static final String EDIT_USER = "creazione_user";
	public static final String EXHIBITORS_LIST = "lista_exhibitor";
	public static final String EXHIBITORS_VIEW = "anagrafica_exhibitor";
	public static final String HEADER = "header";
	public static final String LEFT_COLUMN = "left_column";
	public static final String LOGIN = "login";
	public static final String PASSWORD_LOST = "recupera_password";
	public static final String PROJECT_LIST = "lista_progetti";
	public static final String PROJECT_VIEW = "anagrafica_progetti";
	public static final String USERS_LIST = "lista_user";
	public static final String VIEW_USER = "anagrafica_user";
	
	public static final String CORS_HEADER = "Access-Control-Allow-Origin";
	public static final String USER_LOGIN_COOKIE = "gwcwMappeUserLogin";
	public static final String USER_SESSION = "mappeUser";
	public static final String SSO_REALM = "mido.com";
	//public static final String SSO_REALM = "localhost";
	public static final String PROJECT_ID_FOR_EXHIBITOR_VIEW = "idProject";
	
	public static final String ALL_PARAM = "all";
	public static final String STAND_STATUS_CONFIRMED = "3";
	public static final String USER_ACTIVE = "on";
	public static final int USER_GROUP_ADMIN = 2;
	public static final int USER_GROUP_SUPERUSER = 1;


}
