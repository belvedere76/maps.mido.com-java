<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf8">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <link rel="shortcut icon" type="image/jpeg" href="<c:url value="img/favicon.jpg"/>"/>
    <title>Maps 2.0</title>
    <script src="<c:url value="build.js"/>"></script>
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Lato:300,700" type="text/css">
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="<c:url value="css/designer.css"/>" type="text/css">
  </head>
  <body data-id-utente="<c:out value="${mappeUser.id}"/>" data-id-progetto="<c:out value="${project.id}"/>" data-id-mappa="<c:choose><c:when test="${not empty map}"><c:out value="${map.id}"/></c:when><c:otherwise>0</c:otherwise></c:choose>" data-nome-mappa="<c:out value="${map.name}"/>" data-token="" data-api="http://maps.mido.com">
    <c:import url="header.do">
    	<c:param name="isDesigner" value="${true}"/>
    </c:import>
    <section id="designer">
      <input type="checkbox" class="checkbox"><span class="checkbox"></span>
      <header><a id="save" href="#" class="fa-save"></a>
        <div id="chooseArea">
          <select></select>
        </div><a id="deleteArea" class="fa-trash-o"></a>
        <div id="zoomControl">
          <div id="zoomPin" class="fa-caret-down"></div>
        </div>
        <div id="status">Ready</div>
      </header>
      <div id="svgContainer">
        <svg id="svg" xmlns="http://www.w3.org/2000/svg" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink"></svg>
      </div>
      <div id="leftMenu">
        <div>
          <input id="search" type="text" placeholder="Cerca...">
          <div id="toolPalette">
            <div class="toolrow">
              <div data-active="true" class="tool select"></div>
              <div class="tool ruler"></div>
              <div class="tool bucket"></div>
            </div>
            <div class="toolrow">
              <div class="tool square"></div>
              <div class="tool circle"></div>
              <div class="tool pen"></div>
            </div>
            <div class="toolrow">
              <div class="tool bucketsq"></div>
              <div class="tool rubber"></div>
              <div id="areaElementsTool" class="tool snapshot">
                <ul> </ul>
              </div>
            </div>
            <div class="toolrow">
				<div class="tool c"></div>
				<div class="tool hand"></div>
				<div class="tool pad"></div>
			</div>
          </div>
          <div id="layers">
            <div class="header">
              <h1>Layers</h1>
              <div id="newLayer"></div>
              <div id="removeLayer"></div>
            </div>
            <div class="view">
              <ul id="layersview"></ul>
            </div>
          </div>
        </div>
      </div>
      <div id="statusBar">
        <div id="powered">Powered by GWCW</div>
      </div>
    </section>
  </body>
</html>