<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<!DOCTYPE html>
<html>
  <%@ include file="head.jsp" %>
  <body>
    <c:import url="header.do"/>
    <section>
      <div id="container">
        <c:import url="leftColumn.do">
      		<c:param name="selectedMenu" value="exhibitorsList"/>
      		<c:param name="showUser" value="${true}"/>
      		<c:param name="showProjects" value="${true}"/>
      		<c:param name="showExhibitors" value="${true}"/>
      		<c:param name="customerId" value="${customerId}"/>
      	</c:import>
        <div id="rightColumn">
          <div id="exhibitorProfile"><a href="<c:url value="it/exhibitorsList.do"/><c:out value="${customerId}"/>/all" class="back">Torna a elenco espositori</a>
            <h2><c:out value="${exhibitor.name}"/></h2>
            <!-- Box anagrafica-->
            <div class="box">
              <div class="boxHeader">
                <div class="title">Anagrafica</div>
                <div class="action"><a href="<c:url value="it/deleteExhibitor.do"/><c:out value="${customerId}"/>/<c:out value="${exhibitor.id}"/>">Elimina espositore</a></div>
              </div>
              <div class="boxBody">
                <ul class="col">
                  <li class="row">
                    <div>Indirizzo:</div>
                    <div><c:out value="${exhibitor.address}"/></div>
                  </li>
                  <li class="row">
                    <div>Tel.:</div>
                    <div><c:out value="${exhibitor.tel}"/></div>
                  </li>
                  <li class="row">
                    <div>Fax:</div>
                    <div><c:out value="${exhibitor.fax}"/></div>
                  </li>
                  <li class="row">
                    <div>E-mail:</div>
                    <div><a href="mailto:<c:out value="${exhibitor.email}"/>"><c:out value="${exhibitor.email}"/></a></div>
                  </li>
                </ul>
                <ul class="col">
                </ul>
              </div>
            </div>
            <!-- Box info-->
            <div class="box">
              <div class="boxHeader">
                <div class="title">Informazioni</div>
                <div class="action"></div>
              </div>
              <div class="boxBody">
                <ul class="col">
                	<li class="row">
                    	<div>Rif. progetto:</div>
                    	<div><c:out value="${project.name}" default="-"/></div>
                  	</li>
                	<li class="row">
                    	<div>Posizione:</div>
                    	<div><c:out value="${exhibitor.id3rdParty}" default="-"/></div>
                  	</li>
					<li class="row">
						<div>Posizione prec. :</div>
						<div><c:out value="${exhibitor.lastId3rdParty}" default="-" /></div>
					</li>
					<li class="row">
						<div>MQ Richiesti:</div>
						<div><c:out value="${exhibitor.mqRequested}" /></div>
					</li>
				</ul>
                <ul class="col">
                  <li class="row double">
                    <div>Richiesta cambio:</div>
                    <div>
                      <select name="stato">
                        <option value="1" <c:if test="${exhibitor.changeRequest eq true}">selected="selected"</c:if>>S&igrave;</option>
                        <option value="2" <c:if test="${exhibitor.changeRequest eq false}">selected="selected"</c:if>>No</option>
                      </select>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
            <c:forEach items="${stands}" var="stand">
            	<!-- Box stand-->
	            <div class="box">
	              <div class="boxHeader">
	                <div class="title">Stand associato a questo espositore</div>
	                <div class="action"></div>
	              </div>
	              <div class="boxBody">
	                <ul class="col">
	                	<li class="row">
	                    	<div>Mappa desginer:</div>
	                    	<div><c:out value="${stand.layer.area.map.name}" default="-"/></div>
	                  	</li>
	                  	<li class="row">
	                    	<div>Nome:</div>
	                    	<div><c:out value="${stand.name}" default="-"/></div>
	                  	</li>
	                  	<li class="row">
	                    	<div>Descrizione:</div>
	                    	<div><c:out value="${stand.description}" default="-"/></div>
	                  	</li>
	                  	<li class="row">
	                    	<div>Posizione:</div>
	                    	<div><c:out value="${stand.code}" default="-"/></div>
	                  	</li>
	                  	<li class="row">
	                    	<div>Superficie totale:</div>
	                    	<div><c:out value="${stand.surfaceTot}" default="-"/></div>
	                  	</li>
	                  	<li class="row">
	                    	<div>Superficie effettiva:</div>
	                    	<div><c:out value="${stand.surfaceEffective}" default="-"/></div>
	                  	</li>
	                  	<li class="row">
	                    	<div>Lati aperti:</div>
	                    	<div><c:out value="${stand.openSides}" default="-"/></div>
	                  	</li>
	                </ul>
	                <ul class="col">
	                	<li class="row double">
	                		<div>Logo:</div>
	                    	<div class="double"><c:if test="${not empty stand.logo}"><img src="<c:url value="img/"/><c:out value="${exhibitor.customer.id}"/>/<c:out value="${stand.logo}"/>" alt="Logo"></c:if></div>
	                  	</li>
	                  	<li class="row double">
	                    	<div>Stato:</div>
	                    	<div>
	                      	<select name="stato">
	                      		<c:forEach items="${standStatusList}" var="standStatus">
	                      			<option value="<c:out value="${standStatus.id}"/>" <c:if test="${stand.status.id == standStatus.id}">selected="selected"</c:if>><c:out value="${standStatus.description}"/></option>
	                      		</c:forEach>
	                      	</select>
	                    	</div>
	                  	</li>
	                </ul>
	              </div>
	            </div>
            </c:forEach>
          </div>
        </div>
      </div>
    </section>
  </body>
</html>