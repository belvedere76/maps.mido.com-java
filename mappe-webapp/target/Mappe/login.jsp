<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<!DOCTYPE html>
<html>
  <%@ include file="head.jsp" %>
  <body>
    <c:import url="header.do"/>
    <section>
      <div id="container">
        <div id="leftColumn">
          <div id="loggedOut"><img src="<c:url value="img/login.png"/>">
            <h1>Login</h1>
            <!--p
            <input type="checkbox">&nbsp;Ricordami
            -->
            <p><a href="<c:url value="it/passwordLost.do"/>">Recupera password</a></p>
          </div>
        </div>
        <div id="rightColumn">
          <div id="loginForm">
            <form action="<c:url value="it/login.do"/>" method="POST">
            	<input type="hidden" name="submitted" value="true">
              <ul>
                <li class="row">
                  <label>Email</label>
                  <input type="text" name="email" placeholder="<c:choose><c:when test="${not empty lastEmailLogin}"><c:out value="${lastEmailLogin}"/></c:when><c:otherwise>E-mail</c:otherwise></c:choose>" <c:if test="${not empty lastEmailLogin}">value="<c:out value="${lastEmailLogin}"/>"</c:if>>
                </li>
                <li class="row">
                  <label>Password</label>
                  <input type="password" name="password" placeholder="Password">
                </li>
                <li class="row2">
                  <p>
                    <input type="checkbox" value="true" name="rememberMe" <c:if test="${not empty lastEmailLogin}">checked="checked"</c:if>>&nbsp;Ricordami
                  </p>
                </li>
                <li class="button">
                  <input type="submit" value="Accedi">
                </li>
              </ul>
            </form>
          </div>
        </div>
      </div>
    </section>
  </body>
</html>