<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<head>
	<meta charset="utf-8">
	<link rel="shortcut icon" type="image/jpeg" href="<c:url value="img/favicon.jpg"/>"/>
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Lato:300,700" type="text/css">
	<link rel="stylesheet" href="<c:url value="css/style.css"/>" type="text/css">
	<script type="text/javascript" src="<c:url value="js/slidebox.js"/>"></script>
	<title>Maps 2.0</title>
</head>