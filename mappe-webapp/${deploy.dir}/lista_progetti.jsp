<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<!DOCTYPE html>
<html>
  <%@ include file="head.jsp" %>
  <body>
    <c:import url="header.do"/>
    <section>
      <div id="container">
      	<c:import url="leftColumn.do">
      		<c:param name="selectedMenu" value="projectsList"/>
      		<c:param name="showUser" value="${false}"/>
      		<c:param name="showProjects" value="${true}"/>
      		<c:param name="showExhibitors" value="${false}"/>
      	</c:import>
        <div id="rightColumn">
          <div id="projectList">
          	<c:forEach items="${projects}" var="project">
          		<div class="project">
	              <div class="image"><img src="<c:url value="img/"/><c:out value="${project.id}"/>/<c:out value="${project.img}"/>" alt="placeholder"></div>
	              <div class="row"><span>Creato il</span><span><fmt:formatDate value="${project.creationDate}" pattern="dd/MM/yyyy"/></span></div>
	              <div class="row"><span>Ultima modifica</span><span><c:choose><c:when test="${not empty project.lastEdit}"><fmt:formatDate value="${project.lastEdit}" pattern="dd/MM/yyyy"/></c:when><c:otherwise></c:otherwise></c:choose></span></div><a href="<c:url value="it/projectView.do"/><c:out value="${project.id}"/>/">Apri</a>
	            </div>
          	</c:forEach>
            <c:if test="${mappeUser.userGroup.level == 1}">
            	<!-- <div class="newProject"><a href="<c:url value="it/projectView.do"/>0/">Nuovo progetto</a></div> -->
            </c:if>
          </div>
        </div>
      </div>
    </section>
  </body>
</html>