<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<!DOCTYPE html>
<html>
  <%@ include file="head.jsp" %>
  <body>
    <c:import url="header.do"/>
    <section>
      <div id="container">
        <div id="leftColumn">
          <div id="loggedOut"><img src="<c:url value="img/login.png"/>">
            <h1>Login</h1>
            <!--p
            <input type="checkbox">&nbsp;Ricordami
            -->
            <p><a href="<c:url value="it/login.do"/>">Login</a></p>
          </div>
        </div>
        <div id="rightColumn">
          <div id="passwordRecoveryForm">
            <h1>Recupera password</h1>
            <c:choose>
            	<c:when test="${not empty result and result eq true}">
            		<p>La tua password &egrave; stata inviata all'indirizzo indicato.</p>
            	</c:when>
            	<c:otherwise>
            		<c:if test="${not empty result and result eq false}">
            			<p>Spiacenti, l'indirizzo e-mail indicato non &egrave; presente sui nostri sistemi.</p>
            		</c:if>
            		<p>Inserisci la tua e-mail e ti invieremo una nuova password all'indirizzo indicato.</p>
		            <form action="<c:url value="it/passwordLost.do"/>" method="POST">
		            	<input type="hidden" name="submitted" value="true"/>
		              <ul>
		                <li class="row">
		                  <input type="text" name="email" placeholder="E-mail">
		                </li>
		                <li class="button">
		                  <input type="submit" value="Invia nuova password">
		                </li>
		              </ul>
		            </form>
            	</c:otherwise>
            </c:choose>
          </div>
        </div>
      </div>
    </section>
  </body>
</html>