<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<!DOCTYPE html>
<html>
  <%@ include file="head.jsp" %>
  <body>
    <c:import url="header.do"/>
    <section>
      <div id="container">
        <c:import url="leftColumn.do">
      		<c:param name="selectedMenu" value="${userMenu}"/>
      		<c:param name="showUser" value="${false}"/>
      		<c:param name="showProjects" value="${true}"/>
      		<c:param name="showExhibitors" value="${false}"/>
      	</c:import>
        <div id="rightColumn">
          
          <c:choose>
            	<c:when test="${not empty result and result eq true}">
            		<p>Utente aggiornato con successo.</p>
            	</c:when>
            	<c:when test="${not empty result and result eq false}">
            		<p>Spiacenti, si &egrave; verificato un errore durante il salvataggio dell'utente. Si prega di riprovare pi&ugrave; tardi.</p>
            	</c:when>
            	<c:otherwise>
          
		          <div id="userProfile">
		            <form action="<c:url value="it/editUser.do"/><c:out value="${userView.id}"/>" method="POST">
		            	<input type="hidden" name="submitted" value="true"/>
		              <ul>
		                <li class="row2">
		                  <label>Utente <c:if test="${userView.active eq false}">non </c:if>attivo</label>
		                  <div class="slidebox">
		                    <input type="checkbox" name="active" id="userEnabled" <c:if test="${userView.active}">checked="checked"</c:if>>
		                    <div>
			                    <div class="on" id="userEnabled">No</div>
		                      	<div class="off" id="userEnabled">S&igrave;</div>
		                    </div>
		                  </div>
		                </li>
		                <li class="br"></li>
		                <li class="row">
		                  <label>Email</label>
		                  <input type="text" name="email" placeholder="E-mail" disabled value="<c:out value="${userView.email}"/>">
		                </li>
		                <li class="br"></li>
		                <li class="row">
		                  <label>Data di registrazione</label>
		                  <input type="text" name="registration_date" placeholder="" disabled value="<fmt:formatDate value="${userView.registrationDate}" pattern="dd/MM/yyyy"/>">
		                </li>
		                <li class="row">
		                  <label>Data di attivazione</label>
		                  <input type="text" name="activation_date" placeholder="" disabled value="<fmt:formatDate value="${userView.activationDate}" pattern="dd/MM/yyyy"/>">
		                </li>
		                <li class="row">
		                  <label>Data ultimo accesso</label>
		                  <input type="text" name="last_login" placeholder="" disabled value="<fmt:formatDate value="${userView.lastLogin}" pattern="dd/MM/yyyy"/>">
		                </li>
		                <li class="br"></li>
		                <c:if test="${mappeUser.userGroup.level == 1}">
		                	<li class="row">
			                  <label>Cliente di appartenenza</label>
			                  <select name="customerId">
			                  	<c:forEach items="${customers}" var="customer">
			                  		<option value='<c:out value="${customer.id}"/>' <c:if test="${userView.customer.id==customer.id}">selected="selected"</c:if>><c:out value="${customer.name}"/></option>
			                  	</c:forEach>
			                  </select>
			                </li>
		                </c:if>
		                <li class="row">
		                  <label>Gruppo di appartenenza</label>
		                  <select name="groupId">
		                    <c:forEach items="${groups}" var="group">
		                    	<c:if test="${mappeUser.userGroup.level == 1 || (mappeUser.userGroup.level != 1 && (mappeUser.userGroup.level < group.level))}">
		                  			<option value='<c:out value="${group.id}"/>' <c:if test="${userView.userGroup.level == group.level}">selected="selected"</c:if>><c:out value="${group.name}"/></option>
		                    	</c:if>
		                  	</c:forEach>
		                  </select>
		                </li>
		                <li class="button">
		                  <input type="submit" value="Modifica profilo">
		                </li>
		              </ul>
		            </form>
		          </div>
		         </c:otherwise>
		</c:choose>
        </div>
      </div>
    </section>
  </body>
</html>