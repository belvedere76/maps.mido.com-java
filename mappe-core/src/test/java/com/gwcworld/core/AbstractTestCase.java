package com.gwcworld.core;

import junit.framework.TestCase;

import org.apache.log4j.Logger;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public abstract class AbstractTestCase extends TestCase {

	protected ClassPathXmlApplicationContext factory;

	protected Logger logger = Logger.getLogger(AbstractTestCase.class);
	
	protected void setUp() throws Exception {
		
		String resource = new String("mappe-core-beans.xml");
        factory = new ClassPathXmlApplicationContext(resource);
        
		super.setUp();
		
	}
	
	public void tearDown() throws Exception {
		
	}
}
