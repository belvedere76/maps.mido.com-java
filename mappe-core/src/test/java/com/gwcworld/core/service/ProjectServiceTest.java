package com.gwcworld.core.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwcworld.core.AbstractTestCase;
import com.gwcworld.core.bean.Customer;
import com.gwcworld.core.bean.Project;

public class ProjectServiceTest extends AbstractTestCase {
	
	private Logger logger = LoggerFactory.getLogger(ProjectServiceTest.class);
	private ProjectService service;
	private CustomerService customerService;
	
	public void setUp() throws Exception {
		super.setUp();
		this.service = (ProjectService)factory.getBean("projectService");
		this.customerService = (CustomerService)factory.getBean("customerService");
	}
	
	public void testProjectService(){
		assertNotNull(service);
	}
	
	public void testFindAll(){
		List<Project> projectList = service.getAll();
		logger.debug("N. projects: "+projectList.size());
		assertNotNull(projectList);
	}
	
	public void testFindByCustomer(){
		Customer customer = customerService.getById(1);
		List<Project> projectList = service.getProjectsByCustomer(customer);
		logger.debug("N. projects by customer: "+projectList.size());
		assertNotNull(projectList);
	}
	
	public void tearDown() throws Exception {
		
	}
}


