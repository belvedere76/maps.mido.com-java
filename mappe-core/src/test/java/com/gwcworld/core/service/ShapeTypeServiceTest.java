package com.gwcworld.core.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwcworld.core.AbstractTestCase;
import com.gwcworld.core.bean.ShapeType;


public class ShapeTypeServiceTest extends AbstractTestCase {
	
	private Logger logger = LoggerFactory.getLogger(ShapeTypeServiceTest.class);
	private ShapeTypeService service;
	
	public void setUp() throws Exception {
		super.setUp();
		this.service = (ShapeTypeService)factory.getBean("shapeTypeService");
	}
	
	public void testPaccoTypeService(){
		assertNotNull(service);
	}
	
	public void testFindAll(){
		List<ShapeType> sizeList = service.getAll();
		logger.debug("N.shape type: "+sizeList.size());
		assertNotNull(sizeList);
	}
	
	public void tearDown() throws Exception {
		
	}
}


