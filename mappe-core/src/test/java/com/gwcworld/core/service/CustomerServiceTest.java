package com.gwcworld.core.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwcworld.core.AbstractTestCase;
import com.gwcworld.core.bean.Customer;
import com.gwcworld.core.bean.ShapeType;

public abstract class CustomerServiceTest extends AbstractTestCase {
	
	private Logger logger = LoggerFactory.getLogger(ShapeTypeServiceTest.class);
	private CustomerService service;
	
	public void setUp() throws Exception {
		super.setUp();
		this.service = (CustomerService)factory.getBean("customerService");
	}
	
	public void testCustomerService(){
		assertNotNull(service);
	}
	
	public void testFindById(){
		Customer customer = service.getById(1);
		logger.debug(customer.getName());
		assertNotNull(customer);
	}
	
	public void testFindAll(){
		List<Customer> sizeList = service.getAll();
		logger.debug("N.customers: "+sizeList.size());
		assertNotNull(sizeList);
	}
	
	public void tearDown() throws Exception {
		
	}
}


