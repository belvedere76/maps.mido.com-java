package com.gwcworld.core.service;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwcworld.core.AbstractTestCase;
import com.gwcworld.core.bean.Map;
import com.gwcworld.core.bean.Project;

public abstract class MapServiceTest extends AbstractTestCase {
	
	private Logger logger = LoggerFactory.getLogger(MapServiceTest.class);
	private MapService service;
	private ProjectService projectService;
	
	public void setUp() throws Exception {
		super.setUp();
		this.service = (MapService)factory.getBean("mapService");
		this.projectService = (ProjectService)factory.getBean("projectService");
	}
	
	public void testMapService(){
		assertNotNull(service);
	}
	
	public void testFindById(){
		Map map = service.getById(51);
		logger.debug("Maps: "+map.getDescription());
		assertNotNull(map);
	}
	
	public void testFindAll(){
		List<Map> sizeList = service.getAll();
		logger.debug("N.maps: "+sizeList.size());
		assertNotNull(sizeList);
	}
	
	public void testGetLastSaveDate(){
		Project project = projectService.getById(1);
		Date date = service.getLastSaveDate(project);
		assertNotNull(date);
	}
	
	public void testGetByProjectAndName(){
		Project project = projectService.getById(1);
		List<Map> maps = service.getMapsByProjectAndName(project, "Test Map", 5);
		for (Map map : maps) {
			logger.debug(map.toString());
		}
		assertEquals(2, maps.size());
	}
	
	public void testMapNamesByProject(){
		Project project = projectService.getById(1);
		List<String> names = service.getMapNamesByProject(project);
		for (String name : names) {
			logger.debug(name);
		}
		assertEquals(2, names.size());
	}
	
	public void tearDown() throws Exception {
		
	}
}


