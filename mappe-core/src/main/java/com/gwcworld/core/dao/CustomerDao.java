package com.gwcworld.core.dao;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import com.gwcworld.core.bean.Customer;

public class CustomerDao extends GenericDao<Customer, Integer>{
	
	public Customer getCustomerByToken(String token3rdParty, Session session){
		Criteria criteria = session.createCriteria(Customer.class);
		criteria.add(Restrictions.eq("token3rdParty", token3rdParty));
		return (Customer)criteria.uniqueResult();
	}

}
