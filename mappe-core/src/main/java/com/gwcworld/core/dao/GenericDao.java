package com.gwcworld.core.dao;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public abstract class GenericDao<T, ID extends Serializable> {
	
	private Logger logger = LoggerFactory.getLogger(GenericDao.class);
	private Class<T> persistentClass;
	
	@SuppressWarnings("unchecked")
	public GenericDao(){
		this.persistentClass = (Class<T>)( (ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	}
	
	public List<T> findAll(Session session) {
		return findByCriteria(session, null);
	}

	@SuppressWarnings("unchecked")
	public T findById(ID id, boolean lock, Session session) {
		T result;
		Criteria criteria = session.createCriteria(this.persistentClass);
		criteria.add(Restrictions.eq("id", id));
		result = (T) criteria.uniqueResult();
		return result;
	}
	
	public T findByCode(String code, boolean lock, Session session) {
		T result;
		Criteria criteria = session.createCriteria(this.persistentClass);
		criteria.add(Restrictions.eq("code", code));
		result = (T) criteria.uniqueResult();
		return result;
	}

	@SuppressWarnings("unchecked")
	protected List<T> findByCriteria(Session session, Criterion... criterion){
		List<T> list;
		Criteria criteria = session.createCriteria(persistentClass);
		if( criterion != null ){
			for( Criterion c : criterion ){
				criteria.add(c);
			}			
		}
		list = criteria.list();
		return list;
	}
	
	public void makePersistent(T entity, Session session){
		session.saveOrUpdate(entity);
	}
	
	public void makeTransient(T entity, Session session) {
		session.delete(entity);
	}
	
}
