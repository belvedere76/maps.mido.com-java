package com.gwcworld.core.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import com.gwcworld.core.bean.AreaElement;
import com.gwcworld.core.bean.Layer;

public class AreaElementDao extends GenericDao<AreaElement, Integer>{
	
	public List<AreaElement> getAreaElementListByLayer(Layer layer, Session session){
		Criteria criteria = session.createCriteria(AreaElement.class);
		criteria.add(Restrictions.eq("layer", layer));
		return criteria.list();
	}

}
