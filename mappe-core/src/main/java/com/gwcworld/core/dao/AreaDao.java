package com.gwcworld.core.dao;


import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import com.gwcworld.core.bean.Area;
import com.gwcworld.core.bean.Map;


public class AreaDao extends GenericDao<Area, Integer>{
	
	public List<Area> getAreaListByMap(Map map, Session session){
		Criteria criteria = session.createCriteria(Area.class);
		criteria.add(Restrictions.eq("map", map));
		return criteria.list();
	}
	
	public List<Area> getAreaListByMaps(List<Map> maps, Session session){
		Criteria criteria = session.createCriteria(Area.class);
		criteria.add(Restrictions.in("map", maps));
		return criteria.list();
	}

}
