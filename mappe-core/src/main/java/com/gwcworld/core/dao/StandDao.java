package com.gwcworld.core.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.gwcworld.core.bean.Exhibitor;
import com.gwcworld.core.bean.Layer;
import com.gwcworld.core.bean.Stand;

public class StandDao extends GenericDao<Stand, Integer>{

	public List<Stand> getStandListByExhibitor(Exhibitor exhibitor, Session session){
		Criteria criteria = session.createCriteria(Stand.class);
		criteria.add(Restrictions.eq("exhibitor", exhibitor));
		return criteria.list();
	}
	
	public List<Stand> getStandListByLayer(Layer layer, Session session){
		Criteria criteria = session.createCriteria(Stand.class);
		criteria.add(Restrictions.eq("layer", layer));
		return criteria.list();
	}
	
	public List<Stand> getStandListByExhibitorAndLayers(Exhibitor exhibitor, List<Layer> layers, Session session){
		Criteria criteria = session.createCriteria(Stand.class);
		criteria.add(Restrictions.in("layer", layers));
		criteria.add(Restrictions.eq("exhibitor", exhibitor));
		criteria.addOrder(Order.desc("status"));
		criteria.addOrder(Order.desc("id"));
		return criteria.list();
	}
	
}
