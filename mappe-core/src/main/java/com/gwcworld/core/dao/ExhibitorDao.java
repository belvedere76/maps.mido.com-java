package com.gwcworld.core.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import com.gwcworld.core.bean.Customer;
import com.gwcworld.core.bean.Exhibitor;

public class ExhibitorDao extends GenericDao<Exhibitor, Integer>{
	
	public List<Exhibitor> getExhibitorsByCustomer(Customer customer, Session session){
		return getExhibitorsByCustomerAndName(customer, null, session);
	}
	
	public List<Exhibitor> getExhibitorsByCustomerAndName(Customer customer, String nameInitial, Session session){
		Criteria criteria = session.createCriteria(Exhibitor.class);
		criteria.add(Restrictions.eq("customer", customer));
		if(nameInitial!=null)
			criteria.add(Restrictions.ilike("name", nameInitial, MatchMode.START));
		return criteria.list();
	}
	
	public Exhibitor getExhibitorsByCustomerAndExactName(Customer customer, String name, Session session){
		Criteria criteria = session.createCriteria(Exhibitor.class);
		criteria.add(Restrictions.eq("customer", customer));
		criteria.add(Restrictions.eq("name", name));
		return (Exhibitor)criteria.uniqueResult();
	}

}
