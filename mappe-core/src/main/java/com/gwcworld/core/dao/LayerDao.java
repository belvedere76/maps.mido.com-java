package com.gwcworld.core.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import com.gwcworld.core.bean.Area;
import com.gwcworld.core.bean.Layer;

public class LayerDao extends GenericDao<Layer, Integer>{
	
	public List<Layer> getLayerListByArea(Area area, Session session){
		Criteria criteria = session.createCriteria(Layer.class);
		criteria.add(Restrictions.eq("area", area));
		return criteria.list();
	}
	
	public List<Layer> getLayerListByAreas(List<Area> areas, Session session){
		Criteria criteria = session.createCriteria(Layer.class);
		criteria.add(Restrictions.in("area", areas));
		return criteria.list();
	}

}
