package com.gwcworld.core.dao;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import com.gwcworld.core.bean.Customer;
import com.gwcworld.core.bean.User;

public class UserDao extends GenericDao<User, Integer>{
	
	public User getUserByEmail(String email, Session session){
		Criteria criteria = session.createCriteria(User.class);
		criteria.add(Restrictions.eq("email", email));
		return (User)criteria.uniqueResult();
	}
	
	public User getUserByToken(String optInToken, Session session){
		Criteria criteria = session.createCriteria(User.class);
		criteria.add(Restrictions.eq("optInToken", optInToken));
		return (User)criteria.uniqueResult();
	}
	
	public User getUserByEmailAndPassword(String email, String password, Session session){
		Criteria criteria = session.createCriteria(User.class);
		criteria.add(Restrictions.eq("email", email));
		criteria.add(Restrictions.eq("password", password));
		return (User)criteria.uniqueResult();
	}
	
	public List<User> getUsersByCustomer(Customer customer, Session session){
		return getUsersByCustomerAndName(customer, null, session);
	}
	
	public List<User> getUsersByCustomerAndName(Customer customer, String nameInitial, Session session){
		Criteria criteria = session.createCriteria(User.class);
		criteria.add(Restrictions.eq("customer", customer));
		if(nameInitial!=null)
			criteria.add(Restrictions.ilike("email", nameInitial, MatchMode.START));
		return criteria.list();
	}

	public void updateUserAccesTime(User user, Date currentAccess, Session session){
		Date lastAccess = user.getCurrentLogin()!=null?user.getCurrentLogin():currentAccess;
		user.setLastLogin(lastAccess);
		user.setCurrentLogin(currentAccess);
		makePersistent(user, session);
	}
	
}
