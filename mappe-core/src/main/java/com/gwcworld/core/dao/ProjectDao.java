package com.gwcworld.core.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import com.gwcworld.core.bean.Customer;
import com.gwcworld.core.bean.Project;

public class ProjectDao extends GenericDao<Project, Integer>{
	
	public List<Project> getProjectsByCustomer(Customer customer, Session session){
		Criteria criteria = session.createCriteria(Project.class);
		criteria.add(Restrictions.eq("customer", customer));
		return criteria.list();
	}

}
