package com.gwcworld.core.dao;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import com.gwcworld.core.bean.Map;
import com.gwcworld.core.bean.Project;

public class MapDao extends GenericDao<Map, Integer>{
	
	public List<Map> getMapsByProject(Project project, Session session){
		Criteria criteria = session.createCriteria(Map.class);
		criteria.add(Restrictions.eq("project", project));
		criteria.addOrder(Order.desc("lastEdit"));
		return criteria.list();
	}
	
	public List<Map> getMapsByProjectAndName(Project project, String name, int maxResults, Session session){
		Criteria criteria = session.createCriteria(Map.class);
		criteria.add(Restrictions.eq("project", project));
		criteria.add(Restrictions.eq("name", name));
		criteria.addOrder(Order.desc("lastEdit"));
		criteria.setMaxResults(maxResults);
		return criteria.list();
	}
	
	public Date getLastSaveDate(Project project, Session session){
		Criteria criteria = session.createCriteria(Map.class);
		criteria.add(Restrictions.eq("project", project));
		criteria.addOrder(Order.desc("lastEdit"));
		criteria.setMaxResults(1);
		criteria.setProjection(Projections.property("lastEdit"));
		return (Date)criteria.uniqueResult();
	}
	
	public List<String> getMapNamesByProject(Project project, Session session){
		Criteria criteria = session.createCriteria(Map.class);
		criteria.add(Restrictions.eq("project", project));
		criteria.addOrder(Order.asc("name"));
		criteria.setProjection(Projections.distinct(Projections.property("name")));
		return criteria.list();
	}

}
