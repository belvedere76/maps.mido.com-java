package com.gwcworld.core.service;

import java.util.List;

import com.gwcworld.core.bean.Shape;
import com.gwcworld.core.dao.ShapeDao;

public class ShapeService extends GenericService{

	private ShapeDao shapeDao;
	
	public Shape getById(int id){
		return shapeDao.findById(id, false, getSession());
	}
	
	public List<Shape> getAll(){
		return shapeDao.findAll(getSession());
	}
	
	public void save(Shape shape){
		shapeDao.makePersistent(shape, getSession());
	}

	public void setShapeDao(ShapeDao shapeDao) {
		this.shapeDao = shapeDao;
	}
	
}
