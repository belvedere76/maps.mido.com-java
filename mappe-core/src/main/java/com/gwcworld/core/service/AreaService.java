package com.gwcworld.core.service;

import java.util.List;

import com.gwcworld.core.bean.Area;
import com.gwcworld.core.bean.Map;
import com.gwcworld.core.dao.AreaDao;

public class AreaService extends GenericService{

	private AreaDao areaDao;
	
	public Area getById(int id){
		return areaDao.findById(id, false, getSession());
	}
	
	public List<Area> getAreaListByMap(Map map){
		return areaDao.getAreaListByMap(map, getSession());
	}
	
	public List<Area> getAreaListByMaps(List<Map> maps){
		return areaDao.getAreaListByMaps(maps, getSession());
	}
	
	public List<Area> getAll(){
		return areaDao.findAll(getSession());
	}
	
	public void save(Area area){
		areaDao.makePersistent(area, getSession());
	}

	public void setAreaDao(AreaDao areaDao) {
		this.areaDao = areaDao;
	}
	
}
