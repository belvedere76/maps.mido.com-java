package com.gwcworld.core.service;

import java.util.List;

import com.gwcworld.core.bean.Customer;
import com.gwcworld.core.dao.CustomerDao;

public class CustomerService extends GenericService{

	private CustomerDao customerDao;
	
	public Customer getById(int id){
		return customerDao.findById(id, false, getSession());
	}
	
	public Customer getCustomerByToken(String token3rdParty){
		return customerDao.getCustomerByToken(token3rdParty, getSession());
	}
	
	public List<Customer> getAll(){
		return customerDao.findAll(getSession());
	}
	
	public void save(Customer customer){
		customerDao.makePersistent(customer, getSession());
	}

	public void setCustomerDao(CustomerDao customerDao) {
		this.customerDao = customerDao;
	}
	
}
