package com.gwcworld.core.service;

import java.util.List;

import com.gwcworld.core.bean.UserGroup;
import com.gwcworld.core.dao.UserGroupDao;

public class UserGroupService extends GenericService{

	private UserGroupDao userGroupDao;
	
	public UserGroup getById(int id){
		return userGroupDao.findById(id, false, getSession());
	}
	
	public List<UserGroup> getAll(){
		return userGroupDao.findAll(getSession());
	}
	
	public void save(UserGroup userGroup){
		userGroupDao.makePersistent(userGroup, getSession());
	}

	public void setUserGroupDao(UserGroupDao userGroupDao) {
		this.userGroupDao = userGroupDao;
	}
	
}
