package com.gwcworld.core.service;

import java.util.List;

import com.gwcworld.core.bean.AreaElement;
import com.gwcworld.core.bean.Layer;
import com.gwcworld.core.dao.AreaElementDao;

public class AreaElementService extends GenericService{

	private AreaElementDao areaElementDao;
	
	public AreaElement getById(int id){
		return areaElementDao.findById(id, false, getSession());
	}
	
	public List<AreaElement> getAreaElementListByLayer(Layer layer){
		return areaElementDao.getAreaElementListByLayer(layer, getSession());
	}
	
	public List<AreaElement> getAll(){
		return areaElementDao.findAll(getSession());
	}
	
	public void save(AreaElement areaElement){
		areaElementDao.makePersistent(areaElement, getSession());
	}

	public void setAreaElementDao(AreaElementDao areaElementDao) {
		this.areaElementDao = areaElementDao;
	}
	
}
