package com.gwcworld.core.service;

import java.util.List;

import com.gwcworld.core.bean.ShapeType;
import com.gwcworld.core.dao.ShapeTypeDao;

public class ShapeTypeService extends GenericService{

	private ShapeTypeDao shapeTypeDao;
	
	public ShapeType getById(int id){
		return shapeTypeDao.findById(id, false, getSession());
	}
	
	public List<ShapeType> getAll(){
		return shapeTypeDao.findAll(getSession());
	}
	
	public void save(ShapeType shapeType){
		shapeTypeDao.makePersistent(shapeType, getSession());
	}

	public void setShapeTypeDao(ShapeTypeDao shapeType) {
		this.shapeTypeDao = shapeType;
	}
	
}
