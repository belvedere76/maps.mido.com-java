package com.gwcworld.core.service;

import java.util.List;

import com.gwcworld.core.bean.StandStatus;
import com.gwcworld.core.dao.StandStatusDao;

public class StandStatusService extends GenericService{

	private StandStatusDao standStatusDao;
	
	public StandStatus getById(int id){
		return standStatusDao.findById(id, false, getSession());
	}
	
	public List<StandStatus> getAll(){
		return standStatusDao.findAll(getSession());
	}
	
	public void save(StandStatus standStatus){
		standStatusDao.makePersistent(standStatus, getSession());
	}

	public void setStandStatusDao(StandStatusDao standStatusDao) {
		this.standStatusDao = standStatusDao;
	}
	
}
