package com.gwcworld.core.service;

import java.util.List;

import com.gwcworld.core.bean.Area;
import com.gwcworld.core.bean.Layer;
import com.gwcworld.core.dao.LayerDao;

public class LayerService extends GenericService{

	private LayerDao layerDao;
	
	public Layer getById(int id){
		return layerDao.findById(id, false, getSession());
	}
	
	public List<Layer> getLayerListByArea(Area area){
		return layerDao.getLayerListByArea(area, getSession());
	}
	
	public List<Layer> getLayerListByAreas(List<Area> areas){
		return layerDao.getLayerListByAreas(areas, getSession());
	}
	
	public List<Layer> getAll(){
		return layerDao.findAll(getSession());
	}
	
	public void save(Layer layer){
		layerDao.makePersistent(layer, getSession());
	}

	public void setLayerDao(LayerDao Layer) {
		this.layerDao = Layer;
	}
	
}
