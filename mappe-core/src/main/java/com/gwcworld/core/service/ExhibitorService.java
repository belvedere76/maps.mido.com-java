package com.gwcworld.core.service;

import java.util.List;

import com.gwcworld.core.bean.Customer;
import com.gwcworld.core.bean.Exhibitor;
import com.gwcworld.core.dao.ExhibitorDao;

public class ExhibitorService extends GenericService{

	private ExhibitorDao exhibitorDao;
	
	public Exhibitor getById(int id){
		return exhibitorDao.findById(id, false, getSession());
	}
	
	public List<Exhibitor> getExhibitorsByCustomer(Customer customer){
		return exhibitorDao.getExhibitorsByCustomer(customer, getSession());
	}
	
	public List<Exhibitor> getExhibitorsByCustomerAndName(Customer customer, String nameInitial){
		return exhibitorDao.getExhibitorsByCustomerAndName(customer, nameInitial, getSession());
	}
	
	public Exhibitor getExhibitorsByCustomerAndExactName(Customer customer, String name){
		return exhibitorDao.getExhibitorsByCustomerAndExactName(customer, name, getSession());
	}
	
	public List<Exhibitor> getAll(){
		return exhibitorDao.findAll(getSession());
	}
	
	public void save(Exhibitor exhibitor){
		exhibitorDao.makePersistent(exhibitor, getSession());
	}
	
	public void delete(Exhibitor exhibitor){
		exhibitorDao.makeTransient(exhibitor, getSession());
	}

	public void setExhibitorDao(ExhibitorDao exhibitorDao) {
		this.exhibitorDao = exhibitorDao;
	}
	
}
