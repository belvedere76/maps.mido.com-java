package com.gwcworld.core.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.gwcworld.core.bean.Map;
import com.gwcworld.core.bean.Project;
import com.gwcworld.core.dao.MapDao;

public class MapService extends GenericService{

	private MapDao mapDao;
	
	public Map getById(int id){
		return mapDao.findById(id, false, getSession());
	}
	
	public Date getLastSaveDate(Project project){
		return mapDao.getLastSaveDate(project, getSession());
	}
	
	public List<Map> getMapsByProject(Project project){
		return mapDao.getMapsByProject(project, getSession());
	}
	
	public List<Map> getMapsByProjectAndName(Project project, String name, int maxResults){
		return mapDao.getMapsByProjectAndName(project, name, maxResults, getSession());
	}
	
	public List<String> getMapNamesByProject(Project project){
		return mapDao.getMapNamesByProject(project, getSession());
	}
	
	public List<Map[]> getAllByProjectWithUniqueName(Project project, int maxResults){
		ArrayList<Map[]> mapsByProjectAndUniqueName = new ArrayList<Map[]>();
		List<String> mapNames = mapDao.getMapNamesByProject(project, getSession());
		for (String mapName : mapNames) {
			List<Map> mapList = mapDao.getMapsByProjectAndName(project, mapName, maxResults, getSession());
			Map[] maps = new Map[mapList.size()];
			int pos = 0;
			for (Map map : mapList) {
				maps[pos] = map;
				pos++;
			}
			mapsByProjectAndUniqueName.add(maps);
		}
		return mapsByProjectAndUniqueName;
	}
	
	public List<Map> getAll(){
		return mapDao.findAll(getSession());
	}
	
	public void save(Map map){
		mapDao.makePersistent(map, getSession());
	}

	public void setMapDao(MapDao mapDao) {
		this.mapDao = mapDao;
	}
	
}
