package com.gwcworld.core.service;

import java.util.List;

import com.gwcworld.core.bean.AreaElementType;
import com.gwcworld.core.dao.AreaElementTypeDao;

public class AreaElementTypeService extends GenericService{

	private AreaElementTypeDao areaElementTypeDao;
	
	public AreaElementType getById(int id){
		return areaElementTypeDao.findById(id, false, getSession());
	}
	
	public List<AreaElementType> getAll(){
		return areaElementTypeDao.findAll(getSession());
	}
	
	public void save(AreaElementType areaElementType){
		areaElementTypeDao.makePersistent(areaElementType, getSession());
	}

	public void setAreaElementTypeDao(AreaElementTypeDao areaElementTypeDao) {
		this.areaElementTypeDao = areaElementTypeDao;
	}
	
}
