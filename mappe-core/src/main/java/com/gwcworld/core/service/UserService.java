package com.gwcworld.core.service;

import java.util.Date;
import java.util.List;

import com.gwcworld.core.bean.Customer;
import com.gwcworld.core.bean.User;
import com.gwcworld.core.dao.UserDao;

public class UserService extends GenericService{

	private UserDao userDao;
	
	public User getById(int id){
		return userDao.findById(id, false, getSession());
	}
	
	public User getByEmail(String email){
		return userDao.getUserByEmail(email, getSession());
	}
	
	public User getUserByToken(String optInToken){
		return userDao.getUserByToken(optInToken, getSession());
	}
	
	public User login(String email, String password){
		return userDao.getUserByEmailAndPassword(email, password, getSession());
	}
	
	public List<User> getUsersByCustomer(Customer customer){
		return userDao.getUsersByCustomer(customer, getSession());
	}
	
	public List<User> getUsersByCustomerAndName(Customer customer, String nameInitial){
		return userDao.getUsersByCustomerAndName(customer, nameInitial, getSession());
	}
	
	public void updateUserAccesTime(User user, Date currentAccess){
		userDao.updateUserAccesTime(user, currentAccess, getSession());
	}
	
	public List<User> getAll(){
		return userDao.findAll(getSession());
	}
	
	public void save(User user){
		userDao.makePersistent(user, getSession());
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}
	
}
