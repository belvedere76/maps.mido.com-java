package com.gwcworld.core.service;

import java.util.List;

import com.gwcworld.core.bean.Customer;
import com.gwcworld.core.bean.Project;
import com.gwcworld.core.dao.ProjectDao;

public class ProjectService extends GenericService{

	private ProjectDao projectDao;
	
	public Project getById(int id){
		return projectDao.findById(id, false, getSession());
	}
	
	public List<Project> getProjectsByCustomer(Customer customer){
		return projectDao.getProjectsByCustomer(customer, getSession());
	}
	
	public List<Project> getAll(){
		return projectDao.findAll(getSession());
	}
	
	public void save(Project project){
		projectDao.makePersistent(project, getSession());
	}

	public void setProjectDao(ProjectDao projectDao) {
		this.projectDao = projectDao;
	}
	
}
