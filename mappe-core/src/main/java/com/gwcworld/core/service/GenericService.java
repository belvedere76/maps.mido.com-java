package com.gwcworld.core.service;


import java.util.List;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.orm.hibernate3.SessionFactoryUtils;

import com.gwcworld.core.dao.GenericDao;

public abstract class GenericService {
	
	private Logger logger = LoggerFactory.getLogger(GenericService.class);
	private SessionFactory sessionFactory;
	private Session session;
	private GenericDao genericDao;
	
	public void setGenericDao(GenericDao genericDao) {
		this.genericDao = genericDao;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public Session getSession(){
		
		if (session == null || !session.isOpen()) {
			session = SessionFactoryUtils.doGetSession(sessionFactory, false);
		}
		// logger.debug("Current session " + session.hashCode());
		if (!session.isOpen()) {
			logger.warn("SESSION IS CLOSED");
		}
		
		return session;
	}
	
	public List findAll(){
		return genericDao.findAll(getSession());
	}
	
	public Object findById(Long id){
		return genericDao.findById(id, false, getSession());
	}
	
	public void save( Object entity ){
		genericDao.makePersistent(entity, getSession());
	}
	
}
