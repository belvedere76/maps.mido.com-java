package com.gwcworld.core.service;

import java.util.List;

import com.gwcworld.core.bean.Area;
import com.gwcworld.core.bean.Exhibitor;
import com.gwcworld.core.bean.Layer;
import com.gwcworld.core.bean.Map;
import com.gwcworld.core.bean.Stand;
import com.gwcworld.core.dao.AreaDao;
import com.gwcworld.core.dao.LayerDao;
import com.gwcworld.core.dao.StandDao;

public class StandService extends GenericService{

	private AreaDao areaDao;
	private LayerDao layerDao;
	private StandDao standDao;
	
	public Stand getById(int id){
		return standDao.findById(id, false, getSession());
	}
	
	public List<Stand> getStandListByExhibitor(List<Map> maps, Exhibitor exhibitor){
		List<Area> areas = areaDao.getAreaListByMaps(maps, getSession());
		List<Layer> layers = layerDao.getLayerListByAreas(areas, getSession());		
		return standDao.getStandListByExhibitorAndLayers(exhibitor, layers, getSession());
	}
	
	public List<Stand> getStandListByLayer(Layer layer){
		return standDao.getStandListByLayer(layer, getSession());
	}
	
	public List<Stand> getAll(){
		return standDao.findAll(getSession());
	}
	
	public void save(Stand stand){
		standDao.makePersistent(stand, getSession());
	}

	public void setAreaDao(AreaDao areaDao) {
		this.areaDao = areaDao;
	}

	public void setLayerDao(LayerDao layerDao) {
		this.layerDao = layerDao;
	}

	public void setStandDao(StandDao standDao) {
		this.standDao = standDao;
	}
	
}
