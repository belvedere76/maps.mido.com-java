package com.gwcworld.core.bean;

public class UserGroup {
	
	private int id;
	private String name;
	private String description;
	private byte level;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public byte getLevel() {
		return level;
	}
	public int getLevel2int() {
		Byte byteLevel = level;
		return byteLevel.intValue();
	}
	public void setLevel(byte level) {
		this.level = level;
	}

}
