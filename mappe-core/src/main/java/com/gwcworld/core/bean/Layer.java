package com.gwcworld.core.bean;

import java.util.List;

public class Layer {
	
	private int id;
	private String name;
	private byte zIndex;
	private String imgBackground;
	private boolean visible;
	private Area area;
	private List<AreaElement> areaElements;
	private List<Stand> stands;
	private List<Shape> shapes;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public byte getzIndex() {
		return zIndex;
	}
	public void setzIndex(byte zIndex) {
		this.zIndex = zIndex;
	}
	public String getImgBackground() {
		return imgBackground;
	}
	public void setImgBackground(String imgBackground) {
		this.imgBackground = imgBackground;
	}
	public boolean isVisible() {
		return visible;
	}
	public void setVisible(boolean visible) {
		this.visible = visible;
	}
	public Area getArea() {
		return area;
	}
	public void setArea(Area area) {
		this.area = area;
	}
	public List<AreaElement> getAreaElements() {
		return areaElements;
	}
	public void setAreaElements(List<AreaElement> areaElements) {
		this.areaElements = areaElements;
	}
	public List<Stand> getStands() {
		return stands;
	}
	public void setStands(List<Stand> stands) {
		this.stands = stands;
	}
	public List<Shape> getShapes() {
		return shapes;
	}
	public void setShapes(List<Shape> shapes) {
		this.shapes = shapes;
	}
	
}
