package com.gwcworld.core.bean;

public class Customer {
	
	private int id;
	private String name;
	private String vatNumber;
	private String address;
	private String logo;
	private String token3rdParty;
	private boolean enabled;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getVatNumber() {
		return vatNumber;
	}
	public void setVatNumber(String vatNumber) {
		this.vatNumber = vatNumber;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getLogo() {
		return logo;
	}
	public void setLogo(String logo) {
		this.logo = logo;
	}
	public String getToken3rdParty() {
		return token3rdParty;
	}
	public void setToken3rdParty(String token3rdParty) {
		this.token3rdParty = token3rdParty;
	}
	public boolean isEnabled() {
		return enabled;
	}
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

}
