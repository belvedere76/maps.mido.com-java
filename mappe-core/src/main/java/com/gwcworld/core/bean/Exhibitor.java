package com.gwcworld.core.bean;

public class Exhibitor {
	
	private int id;
	private String id3rdParty;
	private String lastId3rdParty;
	private String name;
	private String address;
	private String tel;
	private String fax;
	private String email;
	private int mqRequested;
	private boolean changeRequest;
	private Customer customer;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getId3rdParty() {
		return id3rdParty;
	}
	public void setId3rdParty(String id3rdParty) {
		this.id3rdParty = id3rdParty;
	}
	public String getLastId3rdParty() {
		return lastId3rdParty;
	}
	public void setLastId3rdParty(String lastId3rdParty) {
		this.lastId3rdParty = lastId3rdParty;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public String getFax() {
		return fax;
	}
	public void setFax(String fax) {
		this.fax = fax;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getMqRequested() {
		return mqRequested;
	}
	public void setMqRequested(int mqRequested) {
		this.mqRequested = mqRequested;
	}
	public boolean isChangeRequest() {
		return changeRequest;
	}
	public void setChangeRequest(boolean changeRequest) {
		this.changeRequest = changeRequest;
	}
	public Customer getCustomer() {
		return customer;
	}
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

}
