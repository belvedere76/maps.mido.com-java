package com.gwcworld.core.bean;

import java.util.List;

public class Area {
	
	private int id;
	private String name;
	private String gridsize;
	private String imgBackground;
	private Shape shape;
	private Map map;
	private List<Layer> layers;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGridsize() {
		return gridsize;
	}
	public void setGridsize(String gridsize) {
		this.gridsize = gridsize;
	}
	public String getImgBackground() {
		return imgBackground;
	}
	public void setImgBackground(String imgBackground) {
		this.imgBackground = imgBackground;
	}
	public Shape getShape() {
		return shape;
	}
	public void setShape(Shape shape) {
		this.shape = shape;
	}
	public Map getMap() {
		return map;
	}
	public void setMap(Map map) {
		this.map = map;
	}
	public List<Layer> getLayers() {
		return layers;
	}
	public void setLayers(List<Layer> layers) {
		this.layers = layers;
	}

}
