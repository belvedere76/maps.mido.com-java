package com.gwcworld.core.bean;

public class AreaElement {

	private int id;
	private short surfaceTot;
	private int translateX;
	private int translateY;
	private float scaleX;
	private float scaleY;
	private float rotate;
	private AreaElementType type;
	private Layer layer;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public short getSurfaceTot() {
		return surfaceTot;
	}
	public void setSurfaceTot(short surfaceTot) {
		this.surfaceTot = surfaceTot;
	}
	public int getTranslateX() {
		return translateX;
	}
	public void setTranslateX(int translateX) {
		this.translateX = translateX;
	}
	public int getTranslateY() {
		return translateY;
	}
	public void setTranslateY(int translateY) {
		this.translateY = translateY;
	}
	public float getScaleX() {
		return scaleX;
	}
	public void setScaleX(float scaleX) {
		this.scaleX = scaleX;
	}
	public float getScaleY() {
		return scaleY;
	}
	public void setScaleY(float scaleY) {
		this.scaleY = scaleY;
	}
	public float getRotate() {
		return rotate;
	}
	public void setRotate(float rotate) {
		this.rotate = rotate;
	}
	public AreaElementType getType() {
		return type;
	}
	public void setType(AreaElementType type) {
		this.type = type;
	}
	public Layer getLayer() {
		return layer;
	}
	public void setLayer(Layer layer) {
		this.layer = layer;
	}
	
}
