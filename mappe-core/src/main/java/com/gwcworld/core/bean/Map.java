package com.gwcworld.core.bean;

import java.util.Date;
import java.util.List;

public class Map {

	private int id;
	private String name;
	private String description;
	private boolean milestone;
	private Date lastEdit;
	private User user;
	private Project project;
	private List<Area> areaList;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public boolean isMilestone() {
		return milestone;
	}
	public void setMilestone(boolean milestone) {
		this.milestone = milestone;
	}
	public Date getLastEdit() {
		return lastEdit;
	}
	public void setLastEdit(Date lastEdit) {
		this.lastEdit = lastEdit;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Project getProject() {
		return project;
	}
	public void setProject(Project project) {
		this.project = project;
	}
	public List<Area> getAreaList() {
		return areaList;
	}
	public void setAreaList(List<Area> areaList) {
		this.areaList = areaList;
	}
	@Override
	public String toString() {
		return "Map [id=" + id + ", name=" + name + ", description="
				+ description + ", milestone=" + milestone + ", lastEdit="
				+ lastEdit + "]";
	}
	
}
