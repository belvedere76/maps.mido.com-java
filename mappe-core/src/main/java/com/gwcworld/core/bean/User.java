package com.gwcworld.core.bean;

import java.util.Date;
import java.util.List;

public class User {
	
	private int id;
	private String email;
	private String password;
	private Date registrationDate;
	private Date activationDate;
	private String optInToken;
	private Customer customer;
	private UserGroup userGroup;
	private Date currentLogin;
	private Date lastLogin;
	private boolean active;
	private List<Project> projects;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Date getRegistrationDate() {
		return registrationDate;
	}
	public void setRegistrationDate(Date registrationDate) {
		this.registrationDate = registrationDate;
	}
	public Date getActivationDate() {
		return activationDate;
	}
	public void setActivationDate(Date activationDate) {
		this.activationDate = activationDate;
	}
	public String getOptInToken() {
		return optInToken;
	}
	public void setOptInToken(String optInToken) {
		this.optInToken = optInToken;
	}
	public Customer getCustomer() {
		return customer;
	}
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	public UserGroup getUserGroup() {
		return userGroup;
	}
	public void setUserGroup(UserGroup userGroup) {
		this.userGroup = userGroup;
	}
	public Date getCurrentLogin() {
		return currentLogin;
	}
	public void setCurrentLogin(Date currentLogin) {
		this.currentLogin = currentLogin;
	}
	public Date getLastLogin() {
		return lastLogin;
	}
	public void setLastLogin(Date lastLogin) {
		this.lastLogin = lastLogin;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public List<Project> getProjects() {
		return projects;
	}
	public void setProjects(List<Project> projects) {
		this.projects = projects;
	}
	
}
